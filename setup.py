
from setuptools import setup, find_packages
from codecs import open
from os import path
import re

# notes from http://www.jeffknupp.com/blog/2013/08/16/open-sourcing-a-python-project-the-right-way/
# and https://packaging.python.org/en/latest/distributing.html#setup-args

here = path.abspath(path.dirname(__file__))

readme = open('README.md','r')
README_TEXT = readme.read()
readme.close()

# http://kfei.logdown.com/posts/202797-4-ways-to-put-the-version-number-into-setuppy-for-your-python-package
module_file = open("little_brother/__init__.py").read()
metadata = dict(re.findall("__([a-z]+)__\s*=\s*'([^']+)'", module_file))

setup(
	name='little-brother',
	version=metadata['version'],
	url="",
	long_description = README_TEXT,
	license='MIT Software License',
	author=metadata['author'],
	# This seems to be required to workaround bugs in how numpy is handled by setuptools
	setup_requires=['numpy','scipy'], 
	install_requires=['scikit-image',
					  'scikit-learn',
					  #'numpy',
					  'yapsy',
					  'pandas',
					  'ipython',
					  'python-igraph',
					  'cython',
					  'networkx',
					  'matplotlib'
					  ],
	author_email=metadata['authoremail'],
	classifiers = [
		'Programming Language :: Python',
		'Development Status :: 3 - Alpha',
		'Intended Audience :: Science/Research',
		'License :: OSI Approved :: MIT License',
		'Topic :: Scientific/Engineering :: Image Recognition'
		],
	packages = find_packages(),
	package_data={'little_brother':['*.cfg','plugins/*.yapsy-plugin','plugins/*.pkl'], },
	entry_points={
		'console_scripts' : ['little-brother=little_brother.little_brother:main']
	}
)


