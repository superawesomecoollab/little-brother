#!/usr/bin/env python
import sys
import os
from core.utils import base_dir

currentdir = base_dir()
sys.path.insert(0,currentdir)
sys.path.insert(0,os.path.join(currentdir,'plugins/'))
sys.path.insert(0,os.path.join(currentdir,'core/'))

from core import plugin_base_classes
import core.plugin_base_classes

# import imp
# f,path,description = imp.find_module("PluginBaseClasses")
# PluginBaseClasses = imp.load_module('PluginBaseClasses',f,path,description)


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import time
import logging 
import logging.handlers

from skimage import *
import cv2

from core.delegate_plugin_manager import DelegatePluginManager
from core.qt_handler import QtHandler
from core.video import Video
from core.blob_tracker import BlobTracker
from core.widgets.blob_tracker_widget import BlobTrackerWidget

from core.ui.ui import Ui_MainWindow
from core.ui.preferences import Ui_Dialog

import numpy as np
import pandas as pd


import ConfigParser

# Memory profiling.
# from guppy import hpy
# h = hpy()

PREPROCESSING = 1
DETECTION = 2
ASSIGNMENT = 3
FILTERING = 4
POSTPROCESSING = 5
OUTPUT = 6
filterTypes = {PREPROCESSING: 'Preprocessing',
			   POSTPROCESSING: 'Postprocessing',
			   DETECTION: 'Detection',
			   ASSIGNMENT: 'Assignment',
			   FILTERING: 'Filtering',
			   OUTPUT: 'Output'}
categories = ['Preprocessing', 'Detection', 'Assignment', 'Filtering',
			  'Postprocessing', 'Output']


class Gui(QMainWindow):
	""" Gui controller class for the blob tracking GUI."""
	def __init__(self, parent=None):
		""" Constructor for the GUI class.
		
		:param parent: the parent widget from Qt.
		"""
		QMainWindow.__init__(self, parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)

		if getattr(sys, 'frozen', False):
			self.dir_root = sys._MEIPASS
		else:
			self.dir_root = os.path.abspath(os.path.dirname(__file__))


		self.settings = ConfigParser.ConfigParser()
		self.settings.read(os.path.join(self.dir_root,"settings.cfg"))
		# Monkey patch the settings object so that plugins can find the settings file.
		self.settings.file_path = os.path.join(self.dir_root,"settings.cfg")

		self.logger = logging.getLogger("Gui")
		self.logger.setLevel(self.settings.get("main", "debug"))
		self.handler = QtHandler(self.ui.loggingTextEdit)
		self.logger.addHandler(self.handler)

		self.pluginlogger = logging.getLogger("yapsy")
		self.pluginlogger.setLevel(self.settings.get("main", "debug"))
		self.pluginlogger.addHandler(self.handler)

		self.ui.playbackSlider.sliderReleased.connect(self.videoSeek)

		self.ui.actionQuit.triggered.connect(self.quit)
		self.ui.actionSave_Image.triggered.connect(self.saveImage)
		self.ui.actionPreferences.triggered.connect(self.preferences)
		self.ui.actionPlay.triggered.connect(self.playVideo)
		self.ui.actionStep.triggered.connect(self.stepVideo)
		self.ui.blobButton.clicked.connect(self.track)
		self.ui.videoButton.clicked.connect(self.loadVideo)
		self.ui.playButton.clicked.connect(self.playVideo)
		self.ui.stepButton.clicked.connect(self.stepVideo)

		self.timerDelay = 40
		self.ui.timerDelayEdit.setText(str(self.timerDelay))
		self.ui.timerDelayEdit.textChanged.connect(self.delayChanged)

		# Tracking model setup
		self.blobTrackerWidget = BlobTrackerWidget()
		self.blobTracker = BlobTracker(widget=self.blobTrackerWidget)

		self.update()

		self.scene = QGraphicsScene()
		self.ui.videoView.setScene(self.scene)

		self.manager = DelegatePluginManager(parent=self,settings=self.settings,directories_list=[os.path.join(self.dir_root,"plugins/")])
		self.manager.setPluginPlaces([os.path.join(self.dir_root,"plugins/")])
		self.manager.setCategoriesFilter({
		  'Preprocessing': plugin_base_classes.IPreprocessingPlugin,
		  'Detection': plugin_base_classes.IDetectionPlugin,
		  'Assignment': plugin_base_classes.IAssignmentPlugin,
		  'Filtering': plugin_base_classes.IFilteringPlugin,
		  'Postprocessing': plugin_base_classes.IPostprocessingPlugin,
		  'Output': plugin_base_classes.IOutputPlugin
		})
		self.manager.collectPlugins()

		# Drag and drop
		self.ui.filterStack.setAcceptDrops(True)
		self.ui.filterStack.setDragEnabled(True)
		self.ui.filterStack.setDragDropMode(QAbstractItemView.InternalMove)
		self.ui.filterStack.setDropIndicatorShown(True)

		self.activatedPlugins = []
		self.loadPlugins()

		self.ui.filterStack.expandAll()
		self.ui.filterStack.itemChanged.connect(self.togglePlugin)
		self.ui.filterStack.itemDoubleClicked.connect(self.pluginOptions)

		self._timer = None
		self.loaded = False
		self.videoFilename = None
		self.tracking = False
		self.logger.info("Setup complete.")

	#TODO: refactor 'filter' to plugin.	
	def loadPlugins(self, reset=False):
		"""Loads available plugins from the /plugin subdrectory.  

		Plugins are loaded in in order by section: preprocessing, detection, assignment, filtering and postprocessing. Plugins enabled by the settings are activated.
		"""
		sections = []
		for label in filterTypes.values():  
			sections.append(QTreeWidgetItem([label],0))
		self.ui.filterStack.addTopLevelItems(sections)

		self.plugin_order = []
		for section in sections:
			label = section.text(0)
			for plugin in self.manager.getPluginsOfCategory(label):
				item = QTreeWidgetItem([plugin.name,
										plugin.description, ''], 0)
				item.setFlags(item.flags() | Qt.ItemIsEnabled)
				item.setFlags(item.flags() & ~Qt.ItemIsDropEnabled)
				try:
					t = self.settings.getint('main', plugin.name)
					item.setCheckState(2, int(self.settings.getint('main',
								   plugin.name)))
					section.addChild(item)
					self.togglePlugin(item, 2)
					self.plugin_order.append(plugin)
				except ConfigParser.NoOptionError:
					self.logger.error("Error: no plugin named %s was found in the settings." % plugin.name)

	def quit(self):
		QApplication.quit()
		sys.exit()

	def saveImage(self):
		"""Saves concatenated image of raw frame and processed frame as .png. 

		The filenames are of the format FlyBlobTrack-%Y%m%d-%H%M%S.png.
		"""
		if not self.loaded: 
			self.logger.warning("Trying to save image with no video loaded!")
		return

		fn = "FlyBlobTrack-"+time.strftime("%Y%m%d-%H%M%S")+".png"
		self.logger.debug("Saving image to %s." % fn)

		# Possible \todo: reshape the image to the currently displayed size.
		image = np.concatenate((self.video1.currentFrame, 
								self.video2.currentFrame), axis=1)
		# Curiously, opencv opens in BGR, but doesn't write that way?
		image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		cv2.imwrite(fn, image)

	def preferences(self):
		"""Presents preferences dialog.
		"""
		revMap = {logging.DEBUG:0,logging.INFO:1,logging.WARNING:2,logging.ERROR:3}
		self.dialog = QDialog()
		self.prefsDialog = Ui_Dialog()
		self.prefsDialog.setupUi(self.dialog)

		self.prefsDialog.debugComboBox.setCurrentIndex(revMap[self.logger.getEffectiveLevel()])

		self.dialog.accepted.connect(self.preferencesDialogAccepted)
		self.dialog.exec_()

	def preferencesDialogAccepted(self):
		"""Handles preferences dialog return values.  Changes values in settings.cfg.
		"""
		loggingLevelsMap = {0: logging.DEBUG, 1: logging.INFO,
						2: logging.WARNING, 3: logging.ERROR}
		self.logger.setLevel(loggingLevelsMap[self.prefsDialog.debugComboBox.currentIndex()])
		self.pluginlogger.setLevel(loggingLevelsMap[self.prefsDialog.debugComboBox.currentIndex()])
		self.settings.set('main', 'debug', loggingLevelsMap[self.prefsDialog.debugComboBox.currentIndex()])
		with open(os.path.join(self.dir_root,'settings.cfg'), 'w') as f:
			self.settings.write(f)


	def togglePlugin(self, item, column):
		"""Toggles plugin activation state.
		
		:param item: A QTreeWidgetItem instance.
		:param column: Integer column value (currently ignored).
		"""
		category = item.parent().text(0)
		plugin = self.manager.getPluginByName(item.text(0), category=category) #returns yapsy's plugin class which has the plugin inside it
		# int argument to checkState is the column number
		if item.checkState(2):
			plugin.plugin_object.activate()
			if plugin.plugin_object.is_activated:
				self.logger.info('Plugin %s (%s) activated.' %
								 (plugin.name, category))
				self.settings.set('main', plugin.name, item.checkState(2))
				with open(os.path.join(self.dir_root,'settings.cfg'), 'w') as f:
					self.settings.write(f)
			if plugin not in self.activatedPlugins:
				self.activatedPlugins.append(plugin)
			else:
				# column 2, state 0 (unchecked)
				item.setCheckState(2, 0)
		else:
			plugin.plugin_object.deactivate()
			self.settings.set('main',plugin.name,item.checkState(2))
			with open(os.path.join(self.dir_root,'settings.cfg'), 'w') as f:
				self.settings.write(f)
			self.logger.info('Plugin %s (%s) deactivated.' % (plugin.name,category))
			if plugin in self.activatedPlugins:
				self.activatedPlugins.remove(plugin)

	def pluginOptions(self,item,column):
		"""Handles presentation of options dialogs from plugins that provide one.
		
		:param item: A QTreeWidgetItem instance.
		:param column: Integer column value (currently ignored).
		""" 		
		category = item.parent().text(0)
		plugin = self.manager.getPluginByName(item.text(0),category=category)
		dialog = plugin.plugin_object.options()
		if dialog:
			dialog.exec_()

	def track(self):
		"""Toggles the visibility of the BlobTrackerWidget.
		"""
		self.logger.info("Tracking")

		if not self.tracking:
			self.blobTrackerWidget.show()
			self.tracking = True
		else:
			self.blobTrackerWidget.hide()
			self.tracking = False

	def loadVideo(self):
		"""Presents a dialog to choose a video file and then loads the result.
			
		This method also activates plugins and triggers background threads for plugins that require full-video processing (e.g. background subtraction calculations).
		"""
		self.logger.info("Loading video")

		self.videoFilename = QFileDialog.getOpenFileName(self, 'Open File', '.')[0]
		if not self.videoFilename:
			self.logger.info("User cancelled - no video loaded.")
			return

		self.ui.playbackSlider.setEnabled(True)

		self.video1 = Video(self.videoFilename)
		self.video2 = Video(self.videoFilename)
		self.loaded = True

		# reset loaded plugins and tracking
		for plugin in self.activatedPlugins:
			plugin.plugin_object.activate()
		self.blobTracker.reset()

		self.threads = [] # list of thread objects.
		for plugin in self.plugin_order:
			try:
				self.threads.append(plugin.plugin_object.startVideo(self.video2))
			except NotImplementedError:
				pass

		self.logger.debug("Received %d threads to execute." % len(self.threads))
		self.logger.debug("Executing threads sequentially.")

		self.play()

		self.currentThread = None
		self._threadtimer = QTimer(self)
		self._threadtimer.timeout.connect(self.checkThreads)
		self._threadtimer.start(500)


	def checkThreads(self):
		"""Handles running threads;  expired threads are cleared from the queue and new ones are started, while existing ones are called to do work.
		"""
		if not self.currentThread or not self.currentThread.isRunning():
			if self.threads:
				self.logger.debug("Starting new thread.")
				self.currentThread = self.threads.pop(0)
				self.currentThread.start()
			else:
				self.logger.debug("Finished all threads.")
				self._threadtimer.stop()
				self._threadtimer = None
				return
		self.currentThread.doWork()

	def videoSeek(self):
		"""Seeks video to the user-selected position chosen by a GUI slider.
		"""
		self.restart = False
		if self._timer:
			self.playVideo()
			self.restart = True

		maxFrame = self.video1.getFrameCount()
		value = int(self.ui.playbackSlider.value())
		newFrame = int((value/100.0)*maxFrame)

		self.video1.seekToFrame(newFrame)
		self.video2.seekToFrame(newFrame)

		for plugin in self.activatedPlugins:
			if hasattr(plugin.plugin_object,'frame'):
				plugin.plugin_object.frame = newFrame

		if self.restart:
			self.playVideo()
		
		
	def stepVideo(self):
		"""Steps a single frame of the video.
		"""
		if not self.loaded:
			self.loadVideo()
		else:
			self.logger.info("Stepping video, frame %d" % self.video1.frameNumber)
			if self._timer:
				self.playVideo()
			self.play()

	def playVideo(self):
		"""Plays the a video continuously, or stops it if one is playing.  Will load a video if none is loaded. 
		"""
		if not self._timer: #if there's no timer running start one
			if not self.loaded:
				self.loadVideo()
			self.logger.info("Beginning playback.")
			self.ui.playButton.setText("Stop video")
			self._timer = QTimer(self)
			self._timer.timeout.connect(self.play)
			self._timer.start(self.timerDelay)
		else: #if there is one, stop it
			self.ui.playButton.setText("Play video")
			self.logger.info("Stopping playback.")
			self._timer.stop()
			self._timer = None

	def play(self):
		"""Loads frames sequentially and processes them using activated plugins.

		Plugins are applied in order by section (preprocessing, then detection, etc.) and then by position in the list, with higher going first.
		"""
		try:
			self.scene.clear()
			self.video1.captureNextFrame()
			self.video2.captureNextFrame()

			self.ui.frameLabel.setText("Frame: " + str(self.video1.frameNumber))

			root = self.ui.filterStack.invisibleRootItem()
			for i in range(root.childCount()):
				category = root.child(i)
				categoryName = category.text(0)
				for j in range(category.childCount()):
					item = category.child(j)
					pluginName = item.text(0)
					plugin = self.manager.getPluginByName(pluginName, category=categoryName)
					if plugin.is_activated:
						if hasattr(plugin.plugin_object, 'rawFrame'):
							self.video2.provideRawFrame(plugin.plugin_object)
						if hasattr(plugin.plugin_object, 'frameCount'):
							self.video2.provideFrameCount(plugin.plugin_object)
						if hasattr(plugin.plugin_object, 'tracksRecord'):
							plugin.plugin_object.tracksRecord = self.blobTracker.getTracks()
						if hasattr(plugin.plugin_object, 'labelstackRecord'):
							plugin.plugin_object.labelstackRecord = self.blobTracker.getLabelstack()
						if hasattr(plugin.plugin_object, 'detections'):
							detections = self.blobTracker.getDetections()
							if detections.empty:
								plugin.plugin_object.detections = pd.DataFrame(columns=self.blobTracker.getColumns("detections"))
								#self.logger.error("Cannot do assignments without detections. Have you enabled a detection plugin?")
							else:
								detections = detections[detections['Frame'] == detections['Frame'].max()]
								plugin.plugin_object.detections = detections
						if hasattr(plugin.plugin_object,'contourPixels'):
							plugin.plugin_object.contourPixels = self.blobTracker.getDetectionPixels()

						self.video2.runFilter(plugin.plugin_object,plugin.name)

			self.image1 = self.video1.convertFrame()
			self.image2 = self.video2.convertFrame()

			self.image1 = self.image1.scaled(self.ui.videoView.width()/2,
										  self.ui.videoView.height())
			self.image2 = self.image2.scaled(self.ui.videoView.width()/2,
										  self.ui.videoView.height())
			self.pm1 = self.scene.addPixmap(self.image1)
			self.pm2 = self.scene.addPixmap(self.image2)
			self.pm1.setOffset(0, 0)
			self.pm2.setOffset(self.image1.width(), 0)
			self.scene.update()

		  # Memory profiling.
		  #print h.heap()

		except TypeError:
			self.logger.error("No frame")
			raise
			

	def delayChanged(self, text):
		"""Changes delay for the video processing timer thread;  lower values (toward 0) are faster but requires more effort.  Higher values may be appropriate for slower machines.
		"""
		try:
			self.timerDelay = int(text)
		except ValueError:
			pass


	def getColumns(self, dfType="tracks"):
		"""Returns a list containing column names from a BlobTracker instance.

		:param dfType: string specifying the type of tuple to be returned. Either "tracks" or "detections".
		:rtype: A list of column names.
		"""
		return self.blobTracker.getColumns(dfType)

	def getDetections(self):
		"""Returns a Pandas dataframe of current blob detections (before assignment).

		:rtype: A Pandas dataframe with detection data (see getColumns).
		"""
		return self.blobTracker.getDetections()

	def recordDetections(self, detections):
		"""Receives detections from detection plugins (e.g. ellipseFitting) and passes them to a BlobTracker instance.

		:param detections: A Pandas dataframe with detections (see getColumns).
		"""
		self.blobTracker.recordDetections(detections)

	def recordDetectionPixels(self, pixels):
		"""Receives detection pixels from a detection plugin (e.g. ellipseFitting) and passes them to a BlobTracker instance.

		:param pixels: A nested list of contour pixels.
		"""
		self.blobTracker.recordDetectionPixels(pixels)

	def recordTracks(self, tracks):
		"""Returns the history of tracks data from a BlobTracker instance.  See BlobTracker for more.

		:rtype: A Pandas dataframe of tracking data.
		"""
		self.blobTracker.recordTracks(tracks)

	def recordLabelstack(self, labels):
		"""Receives label stack information from an assignment plugin (e.g. graphTrack) and passes it to a BlobTracker instance.

		:param labels: A Pandas dataframe with label information (see getColumns for more).
		"""
		self.blobTracker.recordLabelstack(labels)

	def nextID(self):
		"""Returns the next blob ID from a BlobTracker instance.
		"""
		return self.blobTracker.nextID()

def main():
	app = QApplication(sys.argv)
	gui = Gui()
	gui.show()

	sys.exit(app.exec_())


if __name__ == "__main__":
	main()
