# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IPreprocessingPlugin
import logging,cv2

class foregroundExtraction(IPreprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    IPreprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    # Instance variables can go here.
    self.rawFrame = None

  def activate(self):
    super(foregroundExtraction,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false

  def deactivate(self):
    super(foregroundExtraction,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    """ Extracts foregrounds from a binary mask.  Note that for this plugin
    to make sense, it should be run in the pipeline after a background subtraction
    plugin."""
    mask = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    _,mask = cv2.threshold(mask,10,255,cv2.THRESH_BINARY)
    foreground = cv2.bitwise_and(self.rawFrame,self.rawFrame,mask=mask)
    #foreground should already be BGR because it's the raw video data.
    return foreground

  def options(self):
    # Optional method that returns a QDialog object.  See, e.g. adaptiveMedianBGS.py for an example.
    pass
