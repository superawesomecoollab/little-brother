# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IPostprocessingPlugin
import logging
import pandas as pd
import numpy as np
import cv2,scipy,colorsys
import scipy.spatial.distance as sd
import ConfigParser

BLOB_MAX=10
PREVLINES=20

def _get_colors(num_colors):
    HSV_tuples = [(x*1.0/num_colors, 0.5, 0.5) for x in range(num_colors)]
    RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)
    RGB_tuples = map(lambda x: (int(x[0]*255),int(x[1]*255),int(x[2]*255)),RGB_tuples)
    return RGB_tuples


class labelstackAssignment(IPostprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    self.parent = parent
    IPostprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    self.palette = _get_colors(BLOB_MAX) # tune this based on possible number of blobs
    # Instance variables can go here.

  def activate(self):
    super(labelstackAssignment,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false
    self.labelstackRecord = None
    self.tracksRecord = None
    self.frame = 0
    self.delta = 50
    self.drawBlobLabels = False

  def deactivate(self):
    super(labelstackAssignment,self).deactivate()
    # Cleanup code goes here.

  def getPts(self,df):
    return [ tuple(x) for x in df[['CenterX','CenterY']].values]

  def distsDf(self,df1,df2):
    return sd.cdist(self.getPts(df1),self.getPts(df2))

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in
    # RGB.  Use cv2.cvtColor() if necessary.
    self.frame += 1

    if len(self.tracksRecord.index) == 0:
      return image

    lastFrame = max(self.tracksRecord['Frame'])
    lastFrameDF = self.tracksRecord.loc[self.tracksRecord['Frame'] == lastFrame]

    merged = lastFrameDF.loc[lastFrameDF['BlobID'] == -1]
    currlabels = self.labelstackRecord.loc[self.labelstackRecord['CurrentFrame'] == lastFrame]

    if not currlabels.empty and not merged.empty:
      labeldist = self.distsDf(merged,currlabels)
      labelassign = labeldist < self.delta
      dfassign = np.vstack(np.where(labelassign))

      for col in range(dfassign.shape[1]):
        r = dfassign[0,col]; c = dfassign[1,col]
        currlabels.iloc[c]['CenterX'] = merged.iloc[r]['CenterX']
        currlabels.iloc[c]['CenterY'] = merged.iloc[r]['CenterY']
        currlabels.iloc[c]['Frame'] = merged.iloc[r]['Frame']


      currlabels = currlabels[self.parent.getColumns('tracks')]
      lastFrameDF = lastFrameDF.drop(merged.iloc[np.unique(dfassign[0,:])].index)
      lastFrameDF = pd.concat([lastFrameDF,currlabels.iloc[np.unique(dfassign[1,:])]],ignore_index=True)

      lastFrameDF['Frame'] = self.frame
      lastFrameDF.reset_index(drop=True)

    self.parent.recordTracks(lastFrameDF)

    if self.drawBlobLabels:
      image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
      binary = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
      _,binary = cv2.threshold(binary,10,255,cv2.THRESH_BINARY)
      binaryrgb = cv2.cvtColor(binary,cv2.COLOR_GRAY2RGB)
      ids = lastFrameDF
      ids = ids.dropna(subset=['BlobID'])
      h, w = binary.shape[:2]
      mask = np.zeros((h+2, w+2), np.uint8)
      mask[:] = 0

      maskpoints = np.transpose(np.nonzero(binary))
      maskpoints[:,[0,1]] = maskpoints[:,[1,0]]
      # # use a spatial k-d tree to speed up finding nearest coloured pixel mass.
      # # this is all necessary because using the ellipse centroid can miss
      # # the mass of the irregularly shaped blob contours.
      tree = scipy.spatial.KDTree(maskpoints)
      for count, row in ids.iterrows():
        centerx = int(row['CenterX']); centery = int(row['CenterY'])
        dist,indexes = tree.query((centerx,centery))
        if not np.isinf(dist):
          nearestx,nearesty = maskpoints[indexes]
        # colour modulo BLOB_MAX for when new blob ids exceed the number of colours
        (_,image,_,rect) = cv2.floodFill(binaryrgb,mask,(nearestx,nearesty),self.palette[int(row['BlobID']) % BLOB_MAX])
        image = cv2.circle(image,(centerx,centery),3,(255,0,0),-1)
        cv2.putText(image,str(int(row['BlobID'])),
          (centerx,centery),cv2.FONT_HERSHEY_TRIPLEX,3,255)

      # Draw lines for PREVLINES iterations
      for blobid in ids['BlobID']:
        bid = int(blobid)
        if bid != -1:
          prevx = None; prevy = None
          for count,row in self.tracksRecord[self.tracksRecord['BlobID']==bid].tail(PREVLINES).iterrows():
            if prevx and prevy:
              curx = int(row['CenterX']); cury = int(row['CenterY'])
              image = cv2.line(image,(prevx,prevy),
                              (curx,cury),self.palette[bid % BLOB_MAX],2)
              prevx = curx; prevy = cury
            else:
              prevx = int(row['CenterX']); prevy = int(row['CenterY'])

      return cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
    else:
      return image


  def options(self):
    # Optional method that returns a QDialog object.  See, e.g. adaptiveMedianBGS.py for an example.
    pass
