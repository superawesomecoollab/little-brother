# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'backgroundSubtraction2Options.ui'
#
# Created: Tue Jun 16 17:19:15 2015
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_backgroundSubtraction2Options(object):
    def setupUi(self, backgroundSubtraction2Options):
        backgroundSubtraction2Options.setObjectName("backgroundSubtraction2Options")
        backgroundSubtraction2Options.resize(400, 176)
        self.buttonBox = QtWidgets.QDialogButtonBox(backgroundSubtraction2Options)
        self.buttonBox.setGeometry(QtCore.QRect(200, 120, 181, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.shadowsCheckBox = QtWidgets.QCheckBox(backgroundSubtraction2Options)
        self.shadowsCheckBox.setGeometry(QtCore.QRect(20, 80, 121, 20))
        self.shadowsCheckBox.setObjectName("shadowsCheckBox")
        self.historyLabel = QtWidgets.QLabel(backgroundSubtraction2Options)
        self.historyLabel.setGeometry(QtCore.QRect(21, 20, 231, 20))
        self.historyLabel.setWordWrap(True)
        self.historyLabel.setObjectName("historyLabel")
        self.historyEdit = QtWidgets.QLineEdit(backgroundSubtraction2Options)
        self.historyEdit.setGeometry(QtCore.QRect(260, 20, 113, 21))
        self.historyEdit.setObjectName("historyEdit")
        self.learningEdit = QtWidgets.QLineEdit(backgroundSubtraction2Options)
        self.learningEdit.setGeometry(QtCore.QRect(260, 50, 113, 21))
        self.learningEdit.setObjectName("learningEdit")
        self.learningLabel = QtWidgets.QLabel(backgroundSubtraction2Options)
        self.learningLabel.setGeometry(QtCore.QRect(20, 50, 231, 16))
        self.learningLabel.setObjectName("learningLabel")

        self.retranslateUi(backgroundSubtraction2Options)
        self.buttonBox.accepted.connect(backgroundSubtraction2Options.accept)
        self.buttonBox.rejected.connect(backgroundSubtraction2Options.reject)
        QtCore.QMetaObject.connectSlotsByName(backgroundSubtraction2Options)

    def retranslateUi(self, backgroundSubtraction2Options):
        _translate = QtCore.QCoreApplication.translate
        backgroundSubtraction2Options.setWindowTitle(_translate("backgroundSubtraction2Options", "Dialog"))
        self.shadowsCheckBox.setText(_translate("backgroundSubtraction2Options", "Detect shadows"))
        self.historyLabel.setToolTip(_translate("backgroundSubtraction2Options", "Higher values of this should slow down background adaptation (makes slow-moving objects pop more)"))
        self.historyLabel.setText(_translate("backgroundSubtraction2Options", "History (number of frames to track)"))
        self.learningLabel.setText(_translate("backgroundSubtraction2Options", "Learning rate (0 to 1,-1 for default)"))

