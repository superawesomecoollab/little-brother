# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugins/graphTrackOptions.ui'
#
# Created: Mon Oct 20 16:03:44 2014
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GraphTrackOptions(object):
    def setupUi(self, GraphTrackOptions):
        GraphTrackOptions.setObjectName("GraphTrackOptions")
        GraphTrackOptions.resize(292, 179)
        self.buttonBox = QtWidgets.QDialogButtonBox(GraphTrackOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-130, 130, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.label_4 = QtWidgets.QLabel(GraphTrackOptions)
        self.label_4.setGeometry(QtCore.QRect(10, 10, 101, 16))
        self.label_4.setObjectName("label_4")
        self.areaThresholdEdit = QtWidgets.QLineEdit(GraphTrackOptions)
        self.areaThresholdEdit.setGeometry(QtCore.QRect(160, 10, 113, 21))
        self.areaThresholdEdit.setObjectName("areaThresholdEdit")
        self.blobLabelBox = QtWidgets.QCheckBox(GraphTrackOptions)
        self.blobLabelBox.setGeometry(QtCore.QRect(10, 100, 211, 20))
        self.blobLabelBox.setObjectName("blobLabelBox")
        self.distanceThresholdEdit = QtWidgets.QLineEdit(GraphTrackOptions)
        self.distanceThresholdEdit.setGeometry(QtCore.QRect(160, 40, 113, 21))
        self.distanceThresholdEdit.setObjectName("distanceThresholdEdit")
        self.label_5 = QtWidgets.QLabel(GraphTrackOptions)
        self.label_5.setGeometry(QtCore.QRect(10, 40, 131, 16))
        self.label_5.setObjectName("label_5")
        self.decayThresholdEdit = QtWidgets.QLineEdit(GraphTrackOptions)
        self.decayThresholdEdit.setGeometry(QtCore.QRect(160, 70, 113, 21))
        self.decayThresholdEdit.setObjectName("decayThresholdEdit")
        self.label_6 = QtWidgets.QLabel(GraphTrackOptions)
        self.label_6.setGeometry(QtCore.QRect(10, 70, 101, 16))
        self.label_6.setObjectName("label_6")

        self.retranslateUi(GraphTrackOptions)
        self.buttonBox.accepted.connect(GraphTrackOptions.accept)
        self.buttonBox.rejected.connect(GraphTrackOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(GraphTrackOptions)

    def retranslateUi(self, GraphTrackOptions):
        _translate = QtCore.QCoreApplication.translate
        GraphTrackOptions.setWindowTitle(_translate("GraphTrackOptions", "Dialog"))
        self.label_4.setToolTip(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_4.setStatusTip(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_4.setWhatsThis(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_4.setText(_translate("GraphTrackOptions", "Area threshold"))
        self.blobLabelBox.setText(_translate("GraphTrackOptions", "Draw blob labels"))
        self.label_5.setToolTip(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_5.setStatusTip(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_5.setWhatsThis(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_5.setText(_translate("GraphTrackOptions", "Distance threshold"))
        self.label_6.setToolTip(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_6.setStatusTip(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_6.setWhatsThis(_translate("GraphTrackOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_6.setText(_translate("GraphTrackOptions", "Decay threshold"))

