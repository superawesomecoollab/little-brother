# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IDetectionPlugin
import logging,cv2
import numpy as np
import pandas as pd
import ConfigParser


class ellipseFitting(IDetectionPlugin):
  def __init__(self,parent=None,settings=None):
    self.parent = parent
    IDetectionPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    self.loadSettings()

  def loadSettings(self):
    if self.settings:
      self.drawContours = self.settings.getboolean('ellipseFitting','drawContours')
      self.drawEllipses = self.settings.getboolean('ellipseFitting','drawEllipses')
      self.areaThreshold = float(self.settings.get('ellipseFitting','areaThreshold',40))
    else:
      raise ValueError('Settings not found.')
      

  def activate(self):
    super(ellipseFitting,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false
    self.frame = 0
    self.blobValues = pd.DataFrame(columns=self.parent.getColumns(dfType="detections"))
    self.loadSettings()
    self.rawFrame = None
    self.contourPixels = None
    self.contours = None

    ## This plugin only runs under OpenCV 3.
    if not hasattr(cv2,'connectedComponents'):
      self.logger.error('Ellipse fitting requires OpenCV 3.')
      self.is_activated=False


  def deactivate(self):
    super(ellipseFitting,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in
    # RGB.  Use cv2.cvtColor() if necessary.
    self.frame += 1
    grey = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

    (_,contours,hierarchy) = cv2.findContours(grey,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    self.contours = filter(lambda x: cv2.contourArea(x) > self.areaThreshold,contours)
    self.ellipses = [ cv2.fitEllipse(x) for x in self.contours if len(x) > 5]

    if self.drawContours:
      cv2.drawContours(image,self.contours,-1,(255,0,0),2,cv2.LINE_AA)
    if self.drawEllipses:
      for ellipse in self.ellipses:
        cv2.ellipse(image,ellipse,(0,255,0),2)
        #Centroid
        cv2.circle(image,tuple(int(i) for i in ellipse[0]),2,(0,0,255),2)

    blobnumber = 1
    for ellipse in self.ellipses:
      centerx = int(ellipse[0][0]); centery = int(ellipse[0][1])
      semix = int(ellipse[1][0]/2); semiy = int(ellipse[1][1]/2)
      area = np.pi * semix * semiy
      self.blobValues = self.blobValues.append(pd.Series({'Frame':self.frame,
                                        'BlobNumber':blobnumber,
                                        'CenterX':centerx,
                                        'CenterY':centery,
                                        'Height':semiy*2,
                                        'Width':semix*2,
                                        'Angle':ellipse[2],
                                        'Area':area}),ignore_index=True)
      blobnumber += 1
    self.calculateDetectionPixels()

    self.parent.recordDetections(self.detectionsDF())
    self.parent.recordDetectionPixels(self.detectionPixels())
    #return cv2.cvtColor(responseMap,cv2.COLOR_RGB2BGR)
    return image

  def detectionsDF(self):
    return self.blobValues[self.blobValues['Frame']==self.frame]

  def calculateDetectionPixels(self):
    if self.contours:
      self.contourPixels = {}
      responseMap = np.zeros(self.rawFrame.shape,dtype=np.uint8)
      cv2.drawContours(responseMap,self.contours,-1,(255,255,255),-1)

      _,labels = cv2.connectedComponents(cv2.cvtColor(responseMap,cv2.COLOR_RGB2GRAY))

      for i in range(0,labels.max()):
        self.contourPixels[i] = {'BlobNumber':i,'Pixels':np.where(labels==i+1)}

  def detectionPixels(self):
    return self.contourPixels

  def options(self):
    """ Options dialog for the ellipse fitting plugin.

    Ellipse fitting has three options:
      drawContours: option to disable or enable drawing of contours around blobs.
      drawEllipses: option to disable or enable drawing of calculated ellipses.
      area threshold: area, in pixels, that blobs must exceed to be counted.
    """
    from ellipseFittingOptions import Ui_ellipseFittingOptions
    from PyQt5 import QtWidgets,QtGui

    self.dialog = QtWidgets.QDialog()
    self.optionsDialog = Ui_ellipseFittingOptions()
    self.optionsDialog.setupUi(self.dialog)

    self.optionsDialog.areaThresholdEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.areaThresholdEdit.setText(str(self.settings.get('ellipseFitting','areaThreshold',40)))
    self.optionsDialog.ellipseDrawingBox.setChecked(self.settings.getboolean('ellipseFitting','drawEllipses'))
    self.optionsDialog.contourDrawingBox.setChecked(self.settings.getboolean('ellipseFitting','drawContours'))

    self.dialog.accepted.connect(self.dialogAccepted)
    return self.dialog

  def dialogAccepted(self):
    self.settings.set('ellipseFitting','areaThreshold',str(int(self.optionsDialog.areaThresholdEdit.text())))
    self.settings.set('ellipseFitting','drawEllipses',str(self.optionsDialog.ellipseDrawingBox.isChecked()))
    self.settings.set('ellipseFitting','drawContours',str(self.optionsDialog.contourDrawingBox.isChecked()))
    with open(self.settings.file_path,'w') as f:
      self.settings.write(f)
    self.loadSettings()
