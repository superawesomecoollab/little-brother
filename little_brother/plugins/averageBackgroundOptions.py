# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugins/averageBackgroundOptions.ui'
#
# Created: Wed Jun  4 16:28:56 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_averageBackgroundOptions(object):
    def setupUi(self, averageBackgroundOptions):
        averageBackgroundOptions.setObjectName("averageBackgroundOptions")
        averageBackgroundOptions.resize(259, 167)
        self.buttonBox = QtWidgets.QDialogButtonBox(averageBackgroundOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-120, 100, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.sampleEdit = QtWidgets.QLineEdit(averageBackgroundOptions)
        self.sampleEdit.setGeometry(QtCore.QRect(130, 50, 113, 21))
        self.sampleEdit.setText("")
        self.sampleEdit.setObjectName("sampleEdit")
        self.label = QtWidgets.QLabel(averageBackgroundOptions)
        self.label.setGeometry(QtCore.QRect(20, 50, 101, 16))
        self.label.setObjectName("label")

        self.retranslateUi(averageBackgroundOptions)
        self.buttonBox.accepted.connect(averageBackgroundOptions.accept)
        self.buttonBox.rejected.connect(averageBackgroundOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(averageBackgroundOptions)

    def retranslateUi(self, averageBackgroundOptions):
        _translate = QtCore.QCoreApplication.translate
        averageBackgroundOptions.setWindowTitle(_translate("averageBackgroundOptions", "Dialog"))
        self.label.setToolTip(_translate("averageBackgroundOptions", "How many times to sample video."))
        self.label.setStatusTip(_translate("averageBackgroundOptions", "How many times to sample video."))
        self.label.setText(_translate("averageBackgroundOptions", "Sample count :"))

