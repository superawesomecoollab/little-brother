# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IPreprocessingPlugin
import cv2,logging
import numpy as np
import ConfigParser

class Laplacian(IPreprocessingPlugin):
    def __init__(self,parent=None,settings=None):
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.settings = settings
        self.loadSettings()

    def loadSettings(self):
        if self.settings:
            self.kernelSize = int(self.settings.get('laplacian','kernelSize',1))
        else:
            raise ValueError('Settings not found.')


    def activate(self):
      super(Laplacian,self).activate()

    def deactivate(self):
      super(Laplacian,self).deactivate()

    def run(self,image):
        """ Computes the Laplacian of an image

        Args:
          image: An opencv image

        Returns:
          An opencv image.
        """
        # This conversion back and forth is required because
        # QImage doesn't have a native 8-bit grayscale color model
        # and so we need to bring it back to RGB via BGR.
        newframe = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

        newframe = cv2.Laplacian(newframe,cv2.CV_16S,ksize=self.kernelSize)
        newframe = cv2.convertScaleAbs(newframe)

        newframe = cv2.cvtColor(newframe,cv2.COLOR_GRAY2BGR)
        return newframe

    def options(self):
        """Kernel options dialog.

          Options for the kernel.
            element: 0,1,2 = Rect, Cross, Ellipse.
            Kernel size: 2n+1 from 0 to
        """

        from laplacianOptions import Ui_LaplacianOptions
        from PyQt5 import QtWidgets,QtGui

        self.dialog = QtWidgets.QDialog()
        self.optionsDialog = Ui_LaplacianOptions()
        self.optionsDialog.setupUi(self.dialog)

        self.optionsDialog.kernelSlider.setValue((int(self.settings.get('laplacian','kernelSize'))-1)/2)

        self.dialog.accepted.connect(self.dialogAccepted)
        return self.dialog

    def dialogAccepted(self):
        self.settings.set('laplacian','kernelSize',str(2*int(self.optionsDialog.kernelSlider.value())+1))

        with open(self.settings.file_path,'w') as f:
          self.settings.write(f)

        self.loadSettings()
