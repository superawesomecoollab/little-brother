from core.plugin_base_classes import IPreprocessingPlugin
import cv2
import logging

class backgroundSubtract(IPreprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    IPreprocessingPlugin.__init__(self)
    # Earlier versions of openCV don't have this.
    self.logger = logging.getLogger("Gui")
    self.settings = settings

  def activate(self):
    super(backgroundSubtract,self).activate()
    try:
      self.fgbg = cv2.createBackgroundSubtractorMOG()
    except AttributeError:
      self.logger.warning("backgroundSubtract requires openCV 3 and the opencv_contrib modules;  see https://github.com/Itseez/opencv_contrib.")
      self.fgbg = None
      self.is_activated=False

  def deactivate(self):
    super(backgroundSubtract,self).deactivate()

  def run(self,image):
    """ Subtracts backgrounds using openCV's GMM based subtractor.

    Args:
      image: An openCV BGR color image.

    Returns:
      An openCV BGR color image.
    """
    if self.fgbg:
      fgmask = self.fgbg.apply(image)
      image = cv2.bitwise_and(image,image,mask=fgmask)
    return image

  def options(self):
    pass
