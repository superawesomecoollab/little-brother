# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'connectedComponentLabelOptions.ui'
#
# Created: Wed Jun 17 16:31:56 2015
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_connectedComponentLabelOptions(object):
    def setupUi(self, connectedComponentLabelOptions):
        connectedComponentLabelOptions.setObjectName("connectedComponentLabelOptions")
        connectedComponentLabelOptions.resize(382, 188)
        self.buttonBox = QtWidgets.QDialogButtonBox(connectedComponentLabelOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-50, 140, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.frameskipEdit = QtWidgets.QLineEdit(connectedComponentLabelOptions)
        self.frameskipEdit.setGeometry(QtCore.QRect(220, 50, 113, 21))
        self.frameskipEdit.setText("")
        self.frameskipEdit.setObjectName("frameskipEdit")
        self.minsizeEdit = QtWidgets.QLineEdit(connectedComponentLabelOptions)
        self.minsizeEdit.setGeometry(QtCore.QRect(220, 80, 113, 21))
        self.minsizeEdit.setObjectName("minsizeEdit")
        self.label = QtWidgets.QLabel(connectedComponentLabelOptions)
        self.label.setGeometry(QtCore.QRect(20, 50, 91, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(connectedComponentLabelOptions)
        self.label_2.setGeometry(QtCore.QRect(20, 80, 151, 16))
        self.label_2.setObjectName("label_2")
        self.boundingBox = QtWidgets.QCheckBox(connectedComponentLabelOptions)
        self.boundingBox.setGeometry(QtCore.QRect(20, 110, 171, 20))
        self.boundingBox.setObjectName("boundingBox")
        self.colourBox = QtWidgets.QCheckBox(connectedComponentLabelOptions)
        self.colourBox.setGeometry(QtCore.QRect(190, 110, 151, 20))
        self.colourBox.setObjectName("colourBox")

        self.retranslateUi(connectedComponentLabelOptions)
        self.buttonBox.accepted.connect(connectedComponentLabelOptions.accept)
        self.buttonBox.rejected.connect(connectedComponentLabelOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(connectedComponentLabelOptions)

    def retranslateUi(self, connectedComponentLabelOptions):
        _translate = QtCore.QCoreApplication.translate
        connectedComponentLabelOptions.setWindowTitle(_translate("connectedComponentLabelOptions", "Dialog"))
        self.label.setText(_translate("connectedComponentLabelOptions", "Frame skip"))
        self.label_2.setText(_translate("connectedComponentLabelOptions", "Minimum object size"))
        self.boundingBox.setText(_translate("connectedComponentLabelOptions", "Draw bounding boxes"))
        self.colourBox.setText(_translate("connectedComponentLabelOptions", "Colour compoennts"))

