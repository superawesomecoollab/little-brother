import cv2
import numpy as np
import logging
from PyQt5.QtCore import *


class medianWorkerThread(QThread):
    """
    Worker class for Median Background Detection
    """
    backgroundDone = pyqtSignal(object)

    def __init__(self,video,sampleCount=20):
        QThread.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.video = video
        self.background = None

        self.framecount = int(self.video.getFrameCount()) # total frames.
        self.logger.debug("Video has %d frames to process." % self.framecount)
        self.frameCounter = 0

        if sampleCount < 1:
            sampleCount = 1
        if sampleCount > self.framecount:
            sampleCount = self.framecount


        # Load a copy to avoid race conditions between the work and main threads.
        # I don't like it, because it will slow things down, but it's a necessary guard.
        self.cap = cv2.VideoCapture(self.video.filename)
        _,frame1 = self.cap.read()
        self.backsum = np.float32(frame1)
        self.stack = frame1[...,None]
        self.sz = frame1.shape

        self.interval = int(self.framecount) / int(sampleCount)
        self.logger.debug("Interval: %d frames" % self.interval)

 
        # Sample every however many frames
        self.t = range(0,self.framecount,int(self.interval))

    def doWork(self):
        try:
            t = self.t.pop(0)

            #self.logger.debug("%d frames out of %d frames processed: medianBGS" % (t,self.interval))
            try:
                self.cap.set(cv2.CAP_PROP_POS_MSEC,t)
            except AttributeError:
                self.cap.set(cv2.cv.CV_CAP_PROP_POS_MSEC,t)

            _,frame = self.cap.read()
            if type(frame) == type(None):
                raise ValueError
                
            #cv2.accumulate(frame, self.backsum)
            self.stack = np.concatenate((self.stack, frame[...,None]), axis=3)
            self.frameCounter += 1
            self.logger.debug("Processed %d frames in background median" % self.frameCounter)

        except IndexError,ValueError:
            # When the list is empty, it will throw IndexError
            # self.backsum = self.backsum / self.frameCounter
            # self.background = cv2.convertScaleAbs(self.backsum)
            self.background = np.zeros(self.sz, np.uint8)
            for n in range(0,3):
                self.background[:,:,n] = np.median(self.stack[:,:,n,:], axis = 2)
            self.backgroundDone.emit(self.background)
            self.exit()
