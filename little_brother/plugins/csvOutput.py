from core.plugin_base_classes import IOutputPlugin
import logging,csv,os,errno
import pandas as pd
import ConfigParser
import tempfile

class csvOutput(IOutputPlugin):
  def __init__(self,parent=None,settings=None):
    self.parent = parent
    IOutputPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    self.videoName = None
    # Instance variables can go here.

  def activate(self):
    super(csvOutput,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false

    self.trackscolumns = self.parent.getColumns('tracks')
    self.labelcolumns = self.parent.getColumns('labels')
    self.tracksRecord = pd.DataFrame(columns=self.trackscolumns)
    self.labelstackRecord = pd.DataFrame(columns=self.labelcolumns)
    if self.settings:
      self.outputdir = self.settings.get('csvOutput','fndir','.')
    else:
      self.outputdir = None

    # Make the dir if it doesn't exit
    try:
      os.makedirs(self.outputdir)
    except OSError as exception:
      if exception.errno != errno.EEXIST:
        raise      

    tracksFD,self.tracksFN = tempfile.mkstemp(suffix="-tracks.csv",dir=self.outputdir)
    labelsFD,self.labelsFN = tempfile.mkstemp(suffix="-labels.csv",dir=self.outputdir)
    linkFD,self.linkFN     = tempfile.mkstemp(suffix="-link.csv",dir=self.outputdir)

    # self.tracksWriter = csv.DictWriter(tracksFD,fieldnames = self.trackscolumns)
    # self.tracksWriter.writeheader()

    # self.labelWriter = csv.DictWriter(labelsFD,fieldnames=self.labelcolumns)
    # self.labelWriter.writeheader()
    with open(self.tracksFN,"w") as f:
      self.tracksWriter = csv.DictWriter(f,fieldnames = self.trackscolumns)
      self.tracksWriter.writeheader()
    with open(self.labelsFN,"w") as f:
      self.labelWriter = csv.DictWriter(f,fieldnames=self.labelcolumns)
      self.labelWriter.writeheader()
    with open(self.linkFN,"w") as f:
      self.linkWriter = csv.writer(f)
      self.linkWriter.writerow(['Tracks','Labels','Video'])
      self.linkWriter.writerow([self.tracksFN,self.labelsFN,self.videoName])
    os.close(tracksFD)
    os.close(labelsFD)
    os.close(linkFD)


  def deactivate(self):
    super(csvOutput,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in
    # RGB.  Use cv2.cvtColor() if necessary.
    if len(self.tracksRecord.index) > 0:
      with open(self.tracksFN,"a") as f:
        self.tracksWriter = csv.DictWriter(f,fieldnames = self.trackscolumns)
        lastFrame = max(self.tracksRecord['Frame'])
        lastFrameDF = self.tracksRecord[self.tracksRecord['Frame'] == lastFrame]
        self.tracksWriter.writerows(lastFrameDF.to_dict('records'))

      with open(self.labelsFN,"a") as f:
        self.labelWriter = csv.DictWriter(f,fieldnames=self.labelcolumns)
        llastFrame = max(self.labelstackRecord['CurrentFrame']) if not self.labelstackRecord['CurrentFrame'].empty else -1
        if llastFrame == lastFrame:
          # Otherwise, we didn't add anything
          llastFrameDF = self.labelstackRecord[self.labelstackRecord['CurrentFrame'] == llastFrame]
          self.labelWriter.writerows(llastFrameDF.to_dict('records'))

    return image

  def options(self):
    # Optional method that returns a QDialog object.  See, e.g. adaptiveMedianBGS.py for an example.
    pass
