# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugins/adaptiveMedianBGSOptions.ui'
#
# Created: Wed May 21 12:32:23 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AdaptiveMedianBGSOptions(object):
    def setupUi(self, AdaptiveMedianBGSOptions):
        AdaptiveMedianBGSOptions.setObjectName("AdaptiveMedianBGSOptions")
        AdaptiveMedianBGSOptions.resize(272, 286)
        self.buttonBox = QtWidgets.QDialogButtonBox(AdaptiveMedianBGSOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-130, 210, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.lowEdit = QtWidgets.QLineEdit(AdaptiveMedianBGSOptions)
        self.lowEdit.setGeometry(QtCore.QRect(130, 50, 113, 21))
        self.lowEdit.setText("")
        self.lowEdit.setObjectName("lowEdit")
        self.highEdit = QtWidgets.QLineEdit(AdaptiveMedianBGSOptions)
        self.highEdit.setGeometry(QtCore.QRect(130, 80, 113, 21))
        self.highEdit.setObjectName("highEdit")
        self.label = QtWidgets.QLabel(AdaptiveMedianBGSOptions)
        self.label.setGeometry(QtCore.QRect(20, 50, 91, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(AdaptiveMedianBGSOptions)
        self.label_2.setGeometry(QtCore.QRect(20, 80, 101, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(AdaptiveMedianBGSOptions)
        self.label_3.setGeometry(QtCore.QRect(20, 120, 91, 16))
        self.label_3.setObjectName("label_3")
        self.learningRateEdit = QtWidgets.QLineEdit(AdaptiveMedianBGSOptions)
        self.learningRateEdit.setGeometry(QtCore.QRect(130, 120, 113, 21))
        self.learningRateEdit.setObjectName("learningRateEdit")
        self.label_4 = QtWidgets.QLabel(AdaptiveMedianBGSOptions)
        self.label_4.setGeometry(QtCore.QRect(20, 160, 101, 16))
        self.label_4.setObjectName("label_4")
        self.learningFramesEdit = QtWidgets.QLineEdit(AdaptiveMedianBGSOptions)
        self.learningFramesEdit.setGeometry(QtCore.QRect(130, 160, 113, 21))
        self.learningFramesEdit.setObjectName("learningFramesEdit")

        self.retranslateUi(AdaptiveMedianBGSOptions)
        self.buttonBox.accepted.connect(AdaptiveMedianBGSOptions.accept)
        self.buttonBox.rejected.connect(AdaptiveMedianBGSOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(AdaptiveMedianBGSOptions)

    def retranslateUi(self, AdaptiveMedianBGSOptions):
        _translate = QtCore.QCoreApplication.translate
        AdaptiveMedianBGSOptions.setWindowTitle(_translate("AdaptiveMedianBGSOptions", "Dialog"))
        self.label.setText(_translate("AdaptiveMedianBGSOptions", "Low threshold"))
        self.label_2.setText(_translate("AdaptiveMedianBGSOptions", "High threshold"))
        self.label_3.setToolTip(_translate("AdaptiveMedianBGSOptions", "<html><head/><body><p>How often the background model is updated, in frames.</p></body></html>"))
        self.label_3.setText(_translate("AdaptiveMedianBGSOptions", "Learning rate"))
        self.label_4.setToolTip(_translate("AdaptiveMedianBGSOptions", "<html><head/><body><p>Number of beginning frames to compute model over.</p></body></html>"))
        self.label_4.setText(_translate("AdaptiveMedianBGSOptions", "Learning frames"))

