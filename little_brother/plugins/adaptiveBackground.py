import sys

# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IPreprocessingPlugin
import logging
import cv2
import numpy as np
import ConfigParser


def calcThresholds(background):
    """
    Creates thresholds for object detection based on local luminance values.

    For now a ratio of 1:5 background luminance is hard coded, this should be added as a parameter later 
    """
    blue,green,red=cv2.split(background)
    fuzzyBkgrdR = cv2.bilateralFilter(red,20,100,100)
    fuzzyBkgrdG = cv2.bilateralFilter(green,20,100,100)
    fuzzyBkgrdB = cv2.bilateralFilter(blue,20,100,100)
    fuzzyBkgrd = cv2.merge((fuzzyBkgrdB,fuzzyBkgrdG,fuzzyBkgrdR))
    fuzzy=cv2.cvtColor(fuzzyBkgrd,cv2.COLOR_BGR2GRAY)
    thresholds = fuzzy/4
    return thresholds

class adaptiveBackground(IPreprocessingPlugin):
    """
    Create an average background image from frames across a video, to create a target for moving object detection
    """
    def __init__(self,parent=None,settings=None):
        """
        Constructor for the plugin 
        """
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.background = None
        self.settings = settings
        self.loadSettings()

    def loadSettings(self):
        """
        Load settings for the plugin
        """
        if self.settings:
            self.settings.read("settings.cfg")
            self.sampleCount = int(self.settings.get('adaptiveBackground','samplecount',20))
        else:
            raise ValueError('Settings not found.')

    def activate(self):
        super(adaptiveBackground,self).activate()

    def deactivate(self):
        super(adaptiveBackground,self).deactivate()

    def startVideo(self,video):
        from adaptiveBackgroundWorkerThread import adaptiveWorkerThread
    
        self.logger.info("adaptiveBackgroundPlugin beginning video processing thread.")
        self.logger.info("adaptiveBackgroundPlugin using samplecount: %d" % self.sampleCount) 
        self.background = None
        self.thread = adaptiveWorkerThread(video,self.sampleCount)
        self.thread.backgroundDone.connect(self.backgroundReady)
        return self.thread

    def startVideoCL(self,videoFile):
        self.logger = logging.getLogger("CLI")
        self.logger.info("adaptiveBackgroundPlugin using samplecount: %d" % self.sampleCount) 
        cap = cv2.VideoCapture(videoFile)
        _,frame1 = cap.read()
        backsum = np.float32(frame1)
        self.frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        if self.frameCount != self.frameCount:
            # WARNING.    This happens when cv2.CAP_PROP_FRAME_COUNT) returns nan.    It's probably a dangerous assumption,
            # but will work well enough for this purpose.
            self.frameCount = 108000

        interval = self.frameCount / self.sampleCount
        tvals = range(0,self.frameCount,int(interval))
        for t in tvals:
            cap.set(cv2.CAP_PROP_POS_MSEC,t)
            _,frame = cap.read()
            cv2.accumulate(frame,backsum)
            self.logger.info("Have done %d adaptive background processing steps" % t) 
            
        backsum = backsum / len(tvals)
        self.background = cv2.convertScaleAbs(backsum)
        self.thresholds = calcThresholds(self.background)
        #print self.thresholds
    
    def backgroundReady(self, background, thresholds):
        self.background = background
        self.thresholds = thresholds

    def run(self,image):
        if type(self.background) == type(None):
            return image
        else:
            foreground = cv2.cvtColor(cv2.absdiff(image,self.background),cv2.COLOR_BGR2GRAY)
            isFore=foreground > self.thresholds
            isFore.dtype = np.uint8
            self.foregroundmask=isFore*255
            return cv2.cvtColor(self.foregroundmask,cv2.COLOR_GRAY2BGR)

    def options(self):
        """Options dialog for adaptive Background plugin.

        adaptive background has one option
            sampleCount: int value that defines the number of samples taken
        """
        from adaptiveBackgroundOptions import Ui_adaptiveBackgroundOptions
        from PyQt5 import QtWidgets,QtGui

        self.dialog = QtWidgets.QDialog()
        self.optionsDialog = Ui_adaptiveBackgroundOptions()
        self.optionsDialog.setupUi(self.dialog)
        self.optionsDialog.sampleEdit.setValidator(QtGui.QIntValidator())

        self.optionsDialog.sampleEdit.setText(str(self.sampleCount))

        self.dialog.accepted.connect(self.dialogAccepted)
        return self.dialog

    def dialogAccepted(self):
        sampleCount = int(self.optionsDialog.sampleEdit.text())
        self.settings.set('adaptiveBackground','sampleCount',str(sampleCount))
        with open(self.settings.file_path,'w') as f:
            self.settings.write(f)
        self.loadSettings()
