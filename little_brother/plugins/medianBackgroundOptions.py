# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugins/medianBackgroundOptions.ui'
#
# Created: Wed Jun  4 16:28:56 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_medianBackgroundOptions(object):
    def setupUi(self, medianBackgroundOptions):
        medianBackgroundOptions.setObjectName("medianBackgroundOptions")
        medianBackgroundOptions.resize(259, 167)
        self.buttonBox = QtWidgets.QDialogButtonBox(medianBackgroundOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-120, 100, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.sampleEdit = QtWidgets.QLineEdit(medianBackgroundOptions)
        self.sampleEdit.setGeometry(QtCore.QRect(130, 50, 113, 21))
        self.sampleEdit.setText("")
        self.sampleEdit.setObjectName("sampleEdit")
        self.label = QtWidgets.QLabel(medianBackgroundOptions)
        self.label.setGeometry(QtCore.QRect(20, 50, 101, 16))
        self.label.setObjectName("label")

        self.retranslateUi(medianBackgroundOptions)
        self.buttonBox.accepted.connect(medianBackgroundOptions.accept)
        self.buttonBox.rejected.connect(medianBackgroundOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(medianBackgroundOptions)

    def retranslateUi(self, medianBackgroundOptions):
        _translate = QtCore.QCoreApplication.translate
        medianBackgroundOptions.setWindowTitle(_translate("medianBackgroundOptions", "Dialog"))
        self.label.setToolTip(_translate("medianBackgroundOptions", "How many times to sample video."))
        self.label.setStatusTip(_translate("medianBackgroundOptions", "How many times to sample video."))
        self.label.setText(_translate("medianBackgroundOptions", "Sample count :"))

