# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugins/adaptiveBackgroundOptions.ui'
#
# Created: Wed Jun  4 16:28:56 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_adaptiveBackgroundOptions(object):
    def setupUi(self, adaptiveBackgroundOptions):
        adaptiveBackgroundOptions.setObjectName("adaptiveBackgroundOptions")
        adaptiveBackgroundOptions.resize(259, 167)
        self.buttonBox = QtWidgets.QDialogButtonBox(adaptiveBackgroundOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-120, 100, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.sampleEdit = QtWidgets.QLineEdit(adaptiveBackgroundOptions)
        self.sampleEdit.setGeometry(QtCore.QRect(130, 50, 113, 21))
        self.sampleEdit.setText("")
        self.sampleEdit.setObjectName("sampleEdit")
        self.label = QtWidgets.QLabel(adaptiveBackgroundOptions)
        self.label.setGeometry(QtCore.QRect(20, 50, 101, 16))
        self.label.setObjectName("label")

        self.retranslateUi(adaptiveBackgroundOptions)
        self.buttonBox.accepted.connect(adaptiveBackgroundOptions.accept)
        self.buttonBox.rejected.connect(adaptiveBackgroundOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(adaptiveBackgroundOptions)

    def retranslateUi(self, adaptiveBackgroundOptions):
        _translate = QtCore.QCoreApplication.translate
        adaptiveBackgroundOptions.setWindowTitle(_translate("adaptiveBackgroundOptions", "Dialog"))
        self.label.setToolTip(_translate("adaptiveBackgroundOptions", "How many times to sample video."))
        self.label.setStatusTip(_translate("adaptiveBackgroundOptions", "How many times to sample video."))
        self.label.setText(_translate("adaptiveBackgroundOptions", "Sample count :"))

