# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IPreprocessingPlugin
#from core.plugin_base_classes import IPreprocessingPlugin
import logging
import ConfigParser
import cv2
from skimage.segmentation import felzenszwalb
from scipy import ndimage


class segmentationBackground(IPreprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    IPreprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    # Instance variables can go here.
    self.settings = settings
    self.loadSettings()

  def loadSettings(self):
    """
    Load settings for the plugin.
    """
    if self.settings:
        self.scale = int(self.settings.get('segmentationBackground','scale'))
        self.sigma = float(self.settings.get('segmentationBackground','sigma'))
        self.min_size = int(self.settings.get('segmentationBackground','min_size'))
    else:
        raise ValueError('Settings not found.')


  def activate(self):
    super(segmentationBackground,self).activate()
    # Activation code here, e.g. creation of background subtraction model. 
    # If activation fails, set the instance variable .is_activated to false

  def deactivate(self):
    super(segmentationBackground,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in 
    # RGB.  Use cv2.cvtColor() if necessary.
    segments = felzenszwalb(image, scale=self.scale, sigma=self.sigma, min_size=self.min_size)
    #segmentation = ndimage.binary_fill_holes(segments)
    segmentation = segments
    gray = cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)

    self.background = segmentation

    gray[segmentation == 0] = 0
    gray[segmentation != 0] = 255
    self.foregroundmask = gray

    return cv2.cvtColor(self.foregroundmask, cv2.COLOR_GRAY2BGR)

  def options(self):
    # Optional method that returns a QDialog object.  See, e.g. adaptiveMedianBGS.py for an example.
    from PyQt5 import QtWidgets,QtGui
    from segmentationBackgroundOptions import Ui_segmentationBackgroundOptions
    
    self.dialog = QtWidgets.QDialog()
    self.optionsDialog = Ui_segmentationBackgroundOptions()
    self.optionsDialog.setupUi(self.dialog)
    self.optionsDialog.scaleEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.sigmaEdit.setValidator(QtGui.QDoubleValidator())
    self.optionsDialog.minSizeEdit.setValidator(QtGui.QIntValidator())

    self.optionsDialog.scaleEdit.setText(str(self.scale))
    self.optionsDialog.sigmaEdit.setText(str(self.sigma))
    self.optionsDialog.minSizeEdit.setText(str(self.min_size))

    self.dialog.accepted.connect(self.dialogAccepted)

    return self.dialog

  def dialogAccepted(self):
    scale = int(self.optionsDialog.scaleEdit.text())
    sigma = float(self.optionsDialog.sigmaEdit.text())
    min_size = int(self.optionsDialog.minSizeEdit.text())

    self.settings.set('segmentationBackground','scale',str(scale))
    self.settings.set('segmentationBackground','sigma',str(sigma))
    self.settings.set('segmentationBackground','min_size',str(min_size))

    with open(self.settings.file_path,'w') as f:
      self.settings.write(f)
    self.loadSettings()
