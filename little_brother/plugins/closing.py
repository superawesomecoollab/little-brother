# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IPreprocessingPlugin
import cv2
import numpy as np

import logging
import ConfigParser

class closing(IPreprocessingPlugin):
    def __init__(self,parent=None,settings=None):
        """
        Applies a closing transformation to a foreground 
        in order to avoid fragmenting objects at the detection stage
        """
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.settings = settings
        self.loadSettings()

    def loadSettings(self):
        if self.settings:
            self.kernelSize = int(self.settings.get('closing','kernelSize',3))
            self.elementValue = int(self.settings.get('closing','elementValue',0))
            ks = (2*self.kernelSize)+1
            self.kernel = cv2.getStructuringElement(self.elementValue,(ks,ks))
        else:
            raise ValueError('Settings not found.')


    def activate(self):
        super(closing,self).activate()

    def deactivate(self):
        super(closing,self).deactivate()

    def run(self,image):
        """ Applies a closing morphology to an image

        Args:
          image: An opencv image

        Returns:
          An opencv image.
        """
        closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, self.kernel)
        return closing

    def options(self):
        """Kernel options dialog.

          Options for the kernel.
            element: 0,1,2 = Rect, Cross, Ellipse.
            Kernel size: 2n+1 from 0 to 12.
        """
        from kernelOptions import Ui_kernelOptions
        from PyQt5 import QtWidgets,QtGui

        self.dialog = QtWidgets.QDialog()
        self.optionsDialog = Ui_kernelOptions()
        self.optionsDialog.setupUi(self.dialog)

        self.optionsDialog.kernelSlider.setValue(int(self.settings.get('closing','kernelSize',3)))
        self.optionsDialog.elementSlider.setValue(int(self.settings.get('closing','elementValue',0)))

        self.dialog.accepted.connect(self.dialogAccepted)
        return self.dialog

    def dialogAccepted(self):
        self.settings.set('closing','kernelSize',str(int(self.optionsDialog.kernelSlider.value())))
        self.settings.set('closing','elementValue',str(int(self.optionsDialog.elementSlider.value())))

        with open(self.settings.file_path,'w') as f:
          self.settings.write(f)

        self.loadSettings()
