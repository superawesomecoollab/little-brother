import pandas as pd
import numpy as np
import scipy.spatial.distance as sd
import igraph as ig
import logging

import sys
sys.path.append('plugins')
from hungarianAlgorithm import linearAssignment

def duplicates(arr):
  "Returns duplicates in a 1D array"
  s = np.sort(arr,axis=None)
  return np.unique(s[s[1:] == s[:-1]])

class graphAssignment(object):
  def __init__(self,alpha=1100,delta=50):
    self.logger = logging.getLogger("Gui")
    self._alpha = alpha
    self._delta = delta
    self.nextID = -1

  def getPts(self,df):
    return [ tuple(x) for x in df[['CenterX','CenterY']].values]

  def distsDf(self,df1,df2):
    try:
      return sd.cdist(self.getPts(df1),self.getPts(df2))
    except ValueError:
      r = len(df1.index) if len(df1.index) > 0 else 1
      c = len(df2.index) if len(df2.index) > 0 else 1
      mat = np.zeros((r,c))
      mat.fill(999999)
      return mat

  def labelAssign(self,df,c,labelstack):
    labelD = self.distsDf(df,labelstack)
    labelFilter = (labelD < self._delta).any(1)
    labelD = labelD[labelFilter]
    labelAssign = linearAssignment(labelD)
    labelAssign[:,0] = c[labelAssign[:,0]]
    return labelAssign

  def getIndicesFromCluster(self,cl,rmax):
    r = np.array(filter(lambda(x):x < rmax,cl))
    c = np.array([x - rmax for x in filter(lambda(x):x >= rmax,cl)])
    return (r,c)

  def mapIdx(self,i,m):
    return i if i < m else i - m

  def findBridges(self,G):
    #horribly brute force, but it will do for now.
    bridges = []
    deltaG = G.copy()
    for e in G.es:
      if min(G.degree([e.source,e.target])) > 1:
        deltaG = G.copy()
        deltaG.delete_edges(e)
        if not deltaG.is_connected():
          bridges.append(e)
        deltaG.add_edges([(e.source,e.target)])
    return bridges

  def labelstackAssign(self,df1,df2,labelstack,c,newID):
    idx = df2.index.map(lambda x: df2.loc[x,'Area'] < self._alpha and x in c)
    try:
      ids = self.labelAssign(df2.loc[idx],c[idx[c]],labelstack)
      df2.loc[ids[:,0],'BlobID'] = labelstack.loc[ids[:,1],'BlobID'].values
      df2.loc[ids[:,0],'PrevX'] = labelstack.loc[ids[:,1],'CenterX']
      df2.loc[ids[:,0],'PrevY'] = labelstack.loc[ids[:,1],'CenterY']
      labelstack = labelstack.drop(labelstack.index[ids[:,1]])
      labelstack = labelstack.reset_index(drop=True)
    except ValueError:
      # this can happen if a big blog splits into smaller, but all still > ALPHA blobs
      ids = np.array([]).reshape(0,2)
    missing = np.setdiff1d(c,ids[:,0])
    # Did the labelstack contain enough ids to cover the split?
    if len(missing):
      for m in missing:
        if df2.loc[m,'Area'] < self._alpha:
          df2.loc[m,'BlobID'] = newID
          newID += 1
        else:
          df2.loc[m,'BlobID'] = -1
    return (df1,df2,labelstack,newID)

  def track(self,df1,df2,labelstack,nextID):
    dmat = self.distsDf(df1,df2)
    labelstackNew = pd.DataFrame(None,columns=labelstack.columns)

    rmax = dmat.shape[0]
    cmax = dmat.shape[1]

    # Use biconnected components to reduce the graph.
    incidence = (dmat < self._delta).astype(np.uint8)
    g = ig.Graph.Incidence(incidence.tolist())

    clusters = list(g.clusters())

    for cl in filter(lambda x: len(x) > 1,clusters):
      rtmp,ctmp = self.getIndicesFromCluster(cl,rmax)
      submat = dmat[np.ix_(rtmp,ctmp)]
      si = incidence[np.ix_(rtmp,ctmp)]
      sg = ig.Graph.Incidence(si.tolist())
      bridges = self.findBridges(sg)
      while bridges:
        # Greedy minimization
        masked = submat.view(np.ma.MaskedArray)
        # Iterate the bridges.  We eliminate the bridge and minimize the resulting distances.
        minlist = []
        for bridge in bridges:
          start = self.mapIdx(bridge.source,len(rtmp)); end = self.mapIdx(bridge.target,len(rtmp))
          si[start,end] = 0
          masked.mask = si != 1
          minlist.append(masked.sum())
          si[start,end] = 1
        # The minimum of minlist is the best configuration.  This will probably still
        # have errors, but we have to draw the line somewhere.
        b = np.argmin(minlist)
        bridge = bridges[b]
        start = self.mapIdx(bridge.source,len(rtmp)); end = self.mapIdx(bridge.target,len(rtmp))
        incidence[rtmp[start],ctmp[end]] = 0
        submat = dmat[np.ix_(rtmp,ctmp)]
        si = incidence[np.ix_(rtmp,ctmp)]
        sg = ig.Graph.Incidence(si.tolist())
        bridges = self.findBridges(sg)

    # Redo the graph after pre-processing.
    g = ig.Graph.Incidence(incidence.tolist())
    clusters = list(g.clusters())
    actions = []
    for cl in clusters:
      rtmp,ctmp = self.getIndicesFromCluster(cl,rmax)
      if len(rtmp) == 1 or len(ctmp) == 1:
        actions.append((rtmp,ctmp))
      else:
        #Ambiguous
        submat = dmat[np.ix_(rtmp,ctmp)]
        # If the subgraph is complete, then we can use col and rowmin to disambiguate
        colmin = np.argmin(submat,0)
        rowmin = np.argmin(submat,1)
        for d in duplicates(colmin):
          try:
            actions.append((rtmp[np.array([d])],ctmp[np.where(colmin==d)[0]]))
            rtmp = np.delete(rtmp,d)
            ctmp = np.delete(ctmp,np.where(colmin==d))
          except IndexError:
            self.logger.warning('Error disambiguating matrix:\n%s' % str(submat))
        for d in duplicates(rowmin):
          try:
            actions.append((rtmp[np.where(rowmin==d)[0]],ctmp[np.array([d])]))
            ctmp = np.delete(ctmp,d)
            rtmp = np.delete(rtmp,np.where(rowmin==d))
          except IndexError:
            self.logger.warning('Error disambiguating matrix:\n%s' % str(submat))
        amat = dmat[np.ix_(rtmp,ctmp)]
        actions.extend([ (np.array([rtmp[x[0]]]),np.array([ctmp[x[1]]])) for x in linearAssignment(amat) ] )


    #newID = int(pd.concat([df2['BlobID'],df1['BlobID'],labelstack['BlobID']]).max())+1
    newID = nextID
    if newID > self.nextID:
      self.nextID = newID
    else:
      newID = self.nextID
    for a in actions:
      r = a[0]; c = a[1]
      if len(r) < len(c): #split or appear
        if len(r) == 0: # appear.  C SHOULD ONLY BE ONE.
          assert len(c) == 1,'appear with more than one column? %d' % len(c)
          df2.loc[c,'BlobID'] = newID
          newID += 1
        else:
          if not labelstack.empty:
              df1,df2,labelstack,newID = self.labelstackAssign(df1,df2,labelstack,c,newID)
          else:
            newids = np.arange(newID,newID+len(c))
            newID += len(c)
            df2.loc[c,'BlobID'] = newids
      elif len(r) > len(c): #merge or disappear
        if len(c) == 0: #disappear
          assert len(r) == 1, 'disappear with more than one row? %d' % len(r)
          # No need to do anything, the id disappeared
        else:
          df2.loc[c,'BlobID'] = -1 #merging, so push -1 label
          labelstackNew = pd.concat([labelstackNew,
                                    df1[df1.index.map(lambda x: df1.loc[x,'Area'] < self._alpha
                                                                and df1.loc[x,'BlobID'] != -1
                                                                and x in r)]])
      elif len(r) == len(c): #assign
        df2.loc[c,'BlobID'] = df1.loc[r,'BlobID'].values
        df2.loc[c,'PrevX'] = df1.loc[r,'CenterX'].values
        df2.loc[c,'PrevY'] = df1.loc[r,'CenterY'].values

    # There are still some edges cases that the algorithm cannot handle.  If
    # there is a missing ID, we need to patch the next generation.
    for x in df2['BlobID'][pd.isnull(df2['BlobID'])].index:
      df2.loc[x,'BlobID'] = newID
      newID += 1

    # In some edge cases, IDs get left on merged blogs. (For example, when a fly jumps from
    # one merged blob to another).
    c = df2[(df2.BlobID != -1) & (df2.Area > self._alpha)].index.values
    if len(c):
      labelstackNew = pd.concat([labelstackNew,
                                 df2.iloc[c]])
      df2.loc[c,'BlobID'] = -1

    # In some edge cases, -1s get assigned to non-merged blobs
    c = df2['BlobID'][df2['BlobID']==-1].index.values
    if len(c):
      df1,df2,labelstack,newID = self.labelstackAssign(df1,df2,labelstack,np.array(c,dtype=np.uint8),newID)


    df1 = df1.reset_index(drop=True)
    df2 = df2.reset_index(drop=True)
    labelstack = pd.concat([labelstack,labelstackNew])
    labelstack = labelstack.reset_index(drop=True)
    return (df1,df2,labelstack)
