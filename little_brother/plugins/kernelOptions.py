# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'kernelOptions.ui'
#
# Created: Wed Jun 17 16:42:47 2015
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_kernelOptions(object):
    def setupUi(self, kernelOptions):
        kernelOptions.setObjectName("kernelOptions")
        kernelOptions.resize(303, 246)
        self.buttonBox = QtWidgets.QDialogButtonBox(kernelOptions)
        self.buttonBox.setGeometry(QtCore.QRect(80, 190, 161, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.elementSlider = QtWidgets.QSlider(kernelOptions)
        self.elementSlider.setGeometry(QtCore.QRect(120, 30, 160, 22))
        self.elementSlider.setMaximum(2)
        self.elementSlider.setOrientation(QtCore.Qt.Horizontal)
        self.elementSlider.setObjectName("elementSlider")
        self.kernelSlider = QtWidgets.QSlider(kernelOptions)
        self.kernelSlider.setGeometry(QtCore.QRect(120, 120, 160, 22))
        self.kernelSlider.setMinimum(1)
        self.kernelSlider.setMaximum(15)
        self.kernelSlider.setOrientation(QtCore.Qt.Horizontal)
        self.kernelSlider.setObjectName("kernelSlider")
        self.elementLabel = QtWidgets.QLabel(kernelOptions)
        self.elementLabel.setGeometry(QtCore.QRect(20, 30, 62, 16))
        self.elementLabel.setObjectName("elementLabel")
        self.elementExplanationLabel = QtWidgets.QLabel(kernelOptions)
        self.elementExplanationLabel.setGeometry(QtCore.QRect(120, 60, 171, 16))
        self.elementExplanationLabel.setObjectName("elementExplanationLabel")
        self.kernelSizeLabel = QtWidgets.QLabel(kernelOptions)
        self.kernelSizeLabel.setGeometry(QtCore.QRect(20, 120, 81, 16))
        self.kernelSizeLabel.setObjectName("kernelSizeLabel")

        self.retranslateUi(kernelOptions)
        self.buttonBox.accepted.connect(kernelOptions.accept)
        self.buttonBox.rejected.connect(kernelOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(kernelOptions)

    def retranslateUi(self, kernelOptions):
        _translate = QtCore.QCoreApplication.translate
        kernelOptions.setWindowTitle(_translate("kernelOptions", "Kernel parameters"))
        self.elementLabel.setText(_translate("kernelOptions", "Element:"))
        self.elementExplanationLabel.setText(_translate("kernelOptions", "0: Rect 1: Cross 2: Ellipse"))
        self.kernelSizeLabel.setText(_translate("kernelOptions", "Kernel Size"))

