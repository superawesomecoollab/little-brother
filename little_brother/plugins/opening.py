# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IPreprocessingPlugin
import cv2,logging
import numpy as np
import ConfigParser


class Opening(IPreprocessingPlugin):
    def __init__(self,parent=None,settings=None):
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.settings = settings
        self.loadSettings()

    def loadSettings(self):
        if self.settings:
            self.kernelSize = int(self.settings.get('opening','kernelSize'))
            self.elementValue = int(self.settings.get('opening','elementValue'))
            self.kernel = cv2.getStructuringElement(self.elementValue,(self.kernelSize,self.kernelSize))
        else:
            raise ValueError('Settings not found.')


    def activate(self):
      super(Opening,self).activate()

    def deactivate(self):
      super(Opening,self).deactivate()

    def run(self,image):
        """ Applies an opening morphology to an image

        Args:
          image: An opencv image

        Returns:
          An opencv image.
        """
        opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, self.kernel)
        return opening

    def options(self):
        """Kernel options dialog.

          Options for the kernel.
            element: 0,1,2 = Rect, Cross, Ellipse.
            Kernel size: 2n+1 from 0 to
        """
        from kernelOptions import Ui_kernelOptions
        from PyQt5 import QtWidgets,QtGui

        self.dialog = QtWidgets.QDialog()
        self.optionsDialog = Ui_kernelOptions()
        self.optionsDialog.setupUi(self.dialog)

        self.optionsDialog.kernelSlider.setValue(self.settings.getint('opening','kernelSize'))
        self.optionsDialog.elementSlider.setValue(self.settings.getint('opening','elementValue'))

        self.dialog.accepted.connect(self.dialogAccepted)
        return self.dialog

    def dialogAccepted(self):
        self.settings.set('opening','kernelSize',str(int(self.optionsDialog.kernelSlider.value())))
        self.settings.set('opening','elementValue',str(int(self.optionsDialog.elementSlider.value())))

        with open(self.settings.file_path,'w') as f:
          self.settings.write(f)

        self.loadSettings()
