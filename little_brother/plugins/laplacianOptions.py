# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugins/laplacianOptions.ui'
#
# Created: Mon May 12 17:48:47 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_LaplacianOptions(object):
    def setupUi(self, LaplacianOptions):
        LaplacianOptions.setObjectName("LaplacianOptions")
        LaplacianOptions.resize(292, 165)
        self.buttonBox = QtWidgets.QDialogButtonBox(LaplacianOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-110, 100, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.kernelSlider = QtWidgets.QSlider(LaplacianOptions)
        self.kernelSlider.setGeometry(QtCore.QRect(70, 30, 160, 22))
        self.kernelSlider.setMinimum(1)
        self.kernelSlider.setMaximum(12)
        self.kernelSlider.setOrientation(QtCore.Qt.Horizontal)
        self.kernelSlider.setObjectName("kernelSlider")
        self.kernelSizeLabel = QtWidgets.QLabel(LaplacianOptions)
        self.kernelSizeLabel.setGeometry(QtCore.QRect(120, 60, 71, 16))
        self.kernelSizeLabel.setObjectName("kernelSizeLabel")

        self.retranslateUi(LaplacianOptions)
        self.buttonBox.accepted.connect(LaplacianOptions.accept)
        self.buttonBox.rejected.connect(LaplacianOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(LaplacianOptions)

    def retranslateUi(self, LaplacianOptions):
        _translate = QtCore.QCoreApplication.translate
        LaplacianOptions.setWindowTitle(_translate("LaplacianOptions", "Dialog"))
        self.kernelSizeLabel.setText(_translate("LaplacianOptions", "Kernel size"))

