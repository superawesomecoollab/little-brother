# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IPreprocessingPlugin
import cv2
import logging

import ConfigParser

class backgroundSubtract2(IPreprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    IPreprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    self.loadSettings()

  def loadSettings(self):
    if self.settings:
      self.detectShadows = self.settings.getboolean('backgroundSubtract2','detectShadows')
      self.history = int(self.settings.get('backgroundSubtract2','history',100))
      self.learningRate = float(self.settings.get('backgroundSubtract2','learningRate',0.5))
    else:
      raise ValueError('Settings not found.')
      

  def activate(self):
    super(backgroundSubtract2,self).activate()
    try:
      self.fgbg = cv2.createBackgroundSubtractorMOG2(history = self.history,
                                                   detectShadows=self.detectShadows)
    except AttributeError:
      self.logger.warning("backgroundSubtractorMOG2 requires openCV 3 and the opencv_contrib modules;  see https://github.com/Itseez/opencv_contrib.")
      self.fgbg = None
      self.is_activated = False

  def deactivate(self):
    super(backgroundSubtract2,self).deactivate()

  def run(self,image):
    """ Subtracts backgrounds using openCV's GMM based subtractor.

    Args:
      image: An openCV BGR color image.

    Returns:
      An openCV BGR color image.
    """
    if self.fgbg:
      fgmask = self.fgbg.apply(image,learningRate = self.learningRate)
      image = cv2.bitwise_and(image,image,mask=fgmask)
    return image

  def options(self):
    """ Options dialog for the mixture of gaussians background subtractor from openCV.

    backgroundSubtractorMOG2 takes 3 options:
      detectShadows:  bool as to whether to label shadows or not.
      history: int number of recent frames to use as a background model.
      learningRate: decay rate of background model.
    """
    from backgroundSubtraction2Options import Ui_backgroundSubtraction2Options
    from PyQt5 import QtWidgets,QtGui

    self.dialog = QtWidgets.QDialog()
    self.optionsDialog = Ui_backgroundSubtraction2Options()
    self.optionsDialog.setupUi(self.dialog)

    self.optionsDialog.shadowsCheckBox.setChecked(self.settings.getboolean('backgroundSubtract2','detectShadows'))
    self.optionsDialog.historyEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.learningEdit.setValidator(QtGui.QDoubleValidator())

    self.optionsDialog.historyEdit.setText(str(self.settings.get('backgroundSubtract2','history',30)))
    self.optionsDialog.learningEdit.setText(str(self.settings.get('backgroundSubtract2','learningRate',-1)))

    self.dialog.accepted.connect(self.dialogAccepted)
    return self.dialog

  def dialogAccepted(self):
    self.settings.set('backgroundSubtract2','detectShadows',
                            str(self.optionsDialog.shadowsCheckBox.isChecked()))
    self.settings.set('backgroundSubtract2','history',
                            int(self.optionsDialog.historyEdit.text()))
    self.settings.set('backgroundSubtract2','learningRate',
                            float(self.optionsDialog.learningEdit.text()))
    with open(self.settings.file_path,'w') as f:
      self.settings.write(f)

    self.loadSettings()
    self.fgbg = cv2.createBackgroundSubtractorMOG2(history = self.history,
                                                     detectShadows=self.detectShadows)
