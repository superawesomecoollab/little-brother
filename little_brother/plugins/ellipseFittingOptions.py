# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ellipseFittingOptions.ui'
#
# Created: Wed Jun 17 16:30:16 2015
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ellipseFittingOptions(object):
    def setupUi(self, ellipseFittingOptions):
        ellipseFittingOptions.setObjectName("ellipseFittingOptions")
        ellipseFittingOptions.resize(261, 183)
        self.buttonBox = QtWidgets.QDialogButtonBox(ellipseFittingOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-130, 130, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.label_4 = QtWidgets.QLabel(ellipseFittingOptions)
        self.label_4.setGeometry(QtCore.QRect(20, 80, 101, 16))
        self.label_4.setObjectName("label_4")
        self.areaThresholdEdit = QtWidgets.QLineEdit(ellipseFittingOptions)
        self.areaThresholdEdit.setGeometry(QtCore.QRect(130, 80, 113, 21))
        self.areaThresholdEdit.setObjectName("areaThresholdEdit")
        self.ellipseDrawingBox = QtWidgets.QCheckBox(ellipseFittingOptions)
        self.ellipseDrawingBox.setGeometry(QtCore.QRect(10, 40, 211, 20))
        self.ellipseDrawingBox.setObjectName("ellipseDrawingBox")
        self.contourDrawingBox = QtWidgets.QCheckBox(ellipseFittingOptions)
        self.contourDrawingBox.setGeometry(QtCore.QRect(10, 10, 221, 20))
        self.contourDrawingBox.setObjectName("contourDrawingBox")

        self.retranslateUi(ellipseFittingOptions)
        self.buttonBox.accepted.connect(ellipseFittingOptions.accept)
        self.buttonBox.rejected.connect(ellipseFittingOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(ellipseFittingOptions)

    def retranslateUi(self, ellipseFittingOptions):
        _translate = QtCore.QCoreApplication.translate
        ellipseFittingOptions.setWindowTitle(_translate("ellipseFittingOptions", "Dialog"))
        self.label_4.setToolTip(_translate("ellipseFittingOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_4.setStatusTip(_translate("ellipseFittingOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_4.setWhatsThis(_translate("ellipseFittingOptions", "Ellipses below this area (in pixels) will be ignored"))
        self.label_4.setText(_translate("ellipseFittingOptions", "Area threshold"))
        self.ellipseDrawingBox.setText(_translate("ellipseFittingOptions", "Draw calculated ellipses"))
        self.contourDrawingBox.setText(_translate("ellipseFittingOptions", "Draw contours around blobs"))

