# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'derivativeOptions.ui'
#
# Created: Wed Jun 17 16:24:08 2015
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_derivativeOptions(object):
    def setupUi(self, derivativeOptions):
        derivativeOptions.setObjectName("derivativeOptions")
        derivativeOptions.resize(307, 262)
        self.buttonBox = QtWidgets.QDialogButtonBox(derivativeOptions)
        self.buttonBox.setGeometry(QtCore.QRect(-60, 210, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.xCheckBox = QtWidgets.QCheckBox(derivativeOptions)
        self.xCheckBox.setGeometry(QtCore.QRect(50, 40, 101, 20))
        self.xCheckBox.setObjectName("xCheckBox")
        self.yCheckBox = QtWidgets.QCheckBox(derivativeOptions)
        self.yCheckBox.setGeometry(QtCore.QRect(180, 40, 101, 20))
        self.yCheckBox.setObjectName("yCheckBox")
        self.derivativeLabel = QtWidgets.QLabel(derivativeOptions)
        self.derivativeLabel.setGeometry(QtCore.QRect(50, 70, 221, 51))
        self.derivativeLabel.setWordWrap(True)
        self.derivativeLabel.setObjectName("derivativeLabel")
        self.kernelSlider = QtWidgets.QSlider(derivativeOptions)
        self.kernelSlider.setGeometry(QtCore.QRect(80, 140, 160, 22))
        self.kernelSlider.setMinimum(1)
        self.kernelSlider.setMaximum(15)
        self.kernelSlider.setOrientation(QtCore.Qt.Horizontal)
        self.kernelSlider.setObjectName("kernelSlider")
        self.kernelSizeLabel = QtWidgets.QLabel(derivativeOptions)
        self.kernelSizeLabel.setGeometry(QtCore.QRect(130, 170, 71, 16))
        self.kernelSizeLabel.setObjectName("kernelSizeLabel")

        self.retranslateUi(derivativeOptions)
        self.buttonBox.accepted.connect(derivativeOptions.accept)
        self.buttonBox.rejected.connect(derivativeOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(derivativeOptions)

    def retranslateUi(self, derivativeOptions):
        _translate = QtCore.QCoreApplication.translate
        derivativeOptions.setWindowTitle(_translate("derivativeOptions", "Dialog"))
        self.xCheckBox.setText(_translate("derivativeOptions", "X derivative"))
        self.yCheckBox.setText(_translate("derivativeOptions", "Y derivative"))
        self.derivativeLabel.setText(_translate("derivativeOptions", "Note: if both x and y are selected, they will be added together"))
        self.kernelSizeLabel.setText(_translate("derivativeOptions", "Kernel size"))

