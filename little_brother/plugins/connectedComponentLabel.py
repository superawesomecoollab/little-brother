# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IAssignmentPlugin
import numpy as np
import cv2
from skimage import morphology, color, measure
import logging
import ConfigParser

class connectedComponentTracking(IAssignmentPlugin):
  def __init__(self,parent=None,settings=None):
    IAssignmentPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    self.loadSettings()

  def loadSettings(self):
    if self.settings:
      self.minsize = int(self.settings.get('ccomp','minsize',200))
      self.frameskip = int(self.settings.get('ccomp','frameskip',20))
      self.colourComponents = self.settings.getboolean('ccomp','colourComponents')
      self.boundingBoxes = self.settings.getboolean('ccomp','boundingBoxes')
    else:
      raise ValueError('Settings not found.')
      
    self.frame = 1

  def activate(self):
    super(connectedComponentTracking,self).activate()

  def deactivate(self):
    super(connectedComponentTracking,self).deactivate()

  def run(self,image):
    """ Labels connected components in the image.

    Args:
      image: An openCV BGR color image.

    Returns:
      An openCV BGR color image.
    """

    if self.frame % self.frameskip == 1:
      gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
      imagergb = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)

      #labelImage = morphology.label(gray)
      labelImage = measure.label(gray)
      morphology.remove_small_objects(labelImage,in_place=True)

      # This is slow, so I'm leaving it out for now.
      #bg_label = np.bincount(labelImage.flatten()).argmax()
      #labelOverlay = color.label2rgb(labelImage,bg_label=bg_label,image=imagergb)
      if self.colourComponents:
        self.labelOverlay = color.label2rgb(labelImage,bg_label=0,image=imagergb)
        self.labelOverlay = self.labelOverlay*255
        self.labelOverlay = np.uint8(self.labelOverlay)
      else:
        self.labelOverlay = imagergb

      # We subtract one from the number of labels because we
      # don't care about the background.

      cv2.putText(self.labelOverlay,str(len(np.unique(labelImage)-1)),
        (20,40),cv2.FONT_HERSHEY_PLAIN,2,255)


      if self.boundingBoxes:
        for region in measure.regionprops(labelImage):
          try:
            minr, minc, maxr, maxc = region.bbox
            cv2.rectangle(self.labelOverlay,(minc,minr),(maxc,maxr),255,2)
          except:
            pass
    self.frame += 1

    if hasattr(self,'labelOverlay'):
      return cv2.cvtColor(self.labelOverlay,cv2.COLOR_RGB2BGR)
    else:
      return image

  def options(self):
    """Options dialog for the connectedComponentLabel plugin.
      """
    from connectedComponentLabelOptions import Ui_connectedComponentLabelOptions
    from PyQt5 import QtWidgets,QtGui
      
    self.dialog = QtWidgets.QDialog()
    self.optionsDialog = Ui_connectedComponentLabelOptions()
    self.optionsDialog.setupUi(self.dialog)

    self.optionsDialog.frameskipEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.minsizeEdit.setValidator(QtGui.QIntValidator())

    self.optionsDialog.frameskipEdit.setText(str(self.settings.get('ccomp','frameskip',20)))
    self.optionsDialog.minsizeEdit.setText(str(self.settings.get('ccomp','minsize',200)))

    self.optionsDialog.colourBox.setChecked(self.settings.getboolean('ccomp','colourComponents'))
    self.optionsDialog.boundingBox.setChecked(self.settings.getboolean('ccomp','boundingBoxes'))

    self.dialog.accepted.connect(self.dialogAccepted)
    return self.dialog

  def dialogAccepted(self):
    self.settings.set('ccomp','minsize',str(int(self.optionsDialog.minsizeEdit.text())))
    self.settings.set('ccomp','frameskip',str(int(self.optionsDialog.frameskipEdit.text())))

    self.settings.set('ccomp','boundingBoxes',str(bool(self.optionsDialog.boundingBox.isChecked())))
    self.settings.set('ccomp','colourComponents',str(bool(self.optionsDialog.colourBox.isChecked())))

    with open(self.settings.file_path,'w') as f:
      self.settings.write(f)
    self.loadSettings()

    self.initalized = False
