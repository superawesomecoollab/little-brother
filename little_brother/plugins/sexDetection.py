import sys
sys.path.append('./plugins')

# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IPostprocessingPlugin
from core.utils import current_plugins_dir
import logging
import pickle
import cv2
import numpy as np
from dbops import padImages, transformImages, calculateSmax,preprocessImages,getClassifierData
import ConfigParser

class sexDetection(IPostprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    self.parent = parent
    IPostprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    
    # Instance variables can go here.
    self.rawFrame = None;
    self.contourPixels = None;
    self.tracksRecord = None;

  def activate(self):
    super(sexDetection,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false
    with open(current_plugins_dir() + 'merge_classifier.pkl','r') as f:
      self.mergedclf = pickle.load(f)

    with open(current_plugins_dir() + 'flip_classifier.pkl','r') as f:
      self.flipclf = pickle.load(f)

    with open(current_plugins_dir() + 'sex_classifier.pkl','r') as f:
      self.sexclf = pickle.load(f)

    self.trackscolumns = self.parent.getColumns('tracks')
    self.visualize = self.settings.getboolean('sexDetection','visualize')

  def deactivate(self):
    super(sexDetection,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in
    # RGB.  Use cv2.cvtColor() if necessary.
    if len(self.tracksRecord.index) == 0:
      return image

    lastFrame = max(self.tracksRecord['Frame'])
    lastFrameDF = self.tracksRecord.loc[self.tracksRecord['Frame'] == lastFrame]
    images = preprocessImages(self.contourPixels,self.rawFrame,image)


    blobs = np.arange(len(images))
    smax = self.mergedclf.smax
    paddedImages = padImages(images,smax)
    transformedImages = transformImages(paddedImages,smax)
    # put the data in n_samples, n_features format.
    classifierData = getClassifierData(transformedImages)

    predicted = self.mergedclf.predict(classifierData)

    singleIdx = np.array(predicted)
    singleIdx = singleIdx==2

    # No blobs left
    if sum(singleIdx) == 0:
        return image

    classifierData = classifierData[singleIdx,:]
    images = [images[i] for i, elem in enumerate(transformedImages) if singleIdx[i]]
    blobs = np.where(singleIdx)[0]

    smax = self.flipclf.smax
    paddedImages = padImages(images,smax)
    transformedImages = transformImages(paddedImages,smax)

    # put the data in n_samples, n_features format.
    classifierData = getClassifierData(transformedImages)

    flipped = self.flipclf.predict(classifierData)

    for i,elem in enumerate(flipped):
      if elem == 'D':
          # -1 means rotate 180 degrees.  http://docs.opencv.org/modules/core/doc/operations_on_arrays.html#void flip(InputArray src, OutputArray dst, int flipCode)
          transformedImages[i] = cv2.flip(transformedImages[i],-1)

    # put the data in n_samples, n_features format.
    classifierData = getClassifierData(transformedImages)
    sex = self.sexclf.predict(classifierData)

    if self.visualize:
      for index,blob in enumerate(blobs.tolist()):
        pixels = np.vstack(self.contourPixels[blob]['Pixels'])
        minx = np.min(pixels[0]); maxx = np.max(pixels[0])
        miny = np.min(pixels[1]); maxy = np.max(pixels[1])
        cv2.putText(image,str(str(sex[index])+" "+str((miny+maxy)/2)+" "+str((minx+maxx)/2)),((miny+maxy)/2,(minx+maxx)/2),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255))

    for index, blob in enumerate(blobs.tolist()):
      for idx,row in lastFrameDF.iterrows():
        pixels = np.vstack(self.contourPixels[blob]['Pixels'])
        minx = np.min(pixels[1]); maxx = np.max(pixels[1])
        miny = np.min(pixels[0]); maxy = np.max(pixels[0])
        if (minx < row['CenterX'] < maxx) and (miny < row['CenterY'] < maxy):
          lastFrameDF.loc[idx,'Sex'] = sex[index]

    lastFrameDF['Sex'].fillna(-1,inplace=True)

    #lastFrameDF.loc[0:len(results),'Sex'] = results
    self.parent.recordTracks(lastFrameDF)

    return image

  def options(self):
    # Optional method that returns a QDialog object.  See, e.g. adaptiveMedianBGS.py for an example.
    pass
