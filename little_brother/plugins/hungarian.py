# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IAssignmentPlugin
import logging,colorsys,cv2
from hungarianAlgorithm import linearAssignment
import pandas as pd
import numpy as np
import scipy.spatial

PREVLINES=20
BLOB_MAX= 10

# Based off of http://stackoverflow.com/questions/876853/generating-color-ranges-in-python
def _get_colors(num_colors):
    HSV_tuples = [(x*1.0/num_colors, 0.5, 0.5) for x in range(num_colors)]
    RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)
    RGB_tuples = map(lambda x: (int(x[0]*255),int(x[1]*255),int(x[2]*255)),RGB_tuples)
    return RGB_tuples

class hungarian(IAssignmentPlugin):
  def __init__(self,parent=None,settings=None):
    self.parent = parent
    IAssignmentPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.palette = _get_colors(BLOB_MAX) # tune this based on possible number of blobs
    self.settings = settings
    # Instance variables can go here.

  def activate(self):
    super(hungarian,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false
    self.frame = 0
    self.blobTracks = pd.DataFrame(columns=self.parent.getColumns('tracks'))
    self.labelstack = pd.DataFrame(columns=self.parent.getColumns('labels'))
    self.detections = None


  def deactivate(self):
    super(hungarian,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in
    # RGB.  Use cv2.cvtColor() if necessary.

    self.trackBlobs()

    image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
    binary = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    _,binary = cv2.threshold(binary,10,255,cv2.THRESH_BINARY)
    binaryrgb = cv2.cvtColor(binary,cv2.COLOR_GRAY2RGB)
    ids = self.blobTracks[self.blobTracks['Frame'] == self.frame]
    ids = ids.dropna(subset=['BlobID'])
    h, w = binary.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)
    mask[:] = 0

    maskpoints = np.transpose(np.nonzero(binary))
    maskpoints[:,[0,1]] = maskpoints[:,[1,0]]
    # # use a spatial k-d tree to speed up finding nearest coloured pixel mass.
    # # this is all necessary because using the ellipse centroid can miss
    # # the mass of the irregularly shaped blob contours.
    tree = scipy.spatial.KDTree(maskpoints)
    for count, row in ids.iterrows():
      centerx = int(row['CenterX']); centery = int(row['CenterY'])
      dist,indexes = tree.query((centerx,centery))
      if not np.isinf(dist):
        nearestx,nearesty = maskpoints[indexes]
      # colour modulo BLOB_MAX for when new blob ids exceed the number of colours
      (_,image,_,rect) = cv2.floodFill(binaryrgb,mask,(nearestx,nearesty),self.palette[int(row['BlobID']) % BLOB_MAX])
      image = cv2.circle(image,(centerx,centery),3,(255,0,0),-1)
      cv2.putText(image,str(int(row['BlobID'])),
        (centerx,centery),cv2.FONT_HERSHEY_PLAIN,2,255)

    # Draw lines for PREVLINES iterations
    for blobid in ids['BlobID']:
      bid = int(blobid)
      prevx = None; prevy = None
      for count,row in self.blobTracks[self.blobTracks['BlobID']==bid].tail(PREVLINES).iterrows():
        if prevx and prevy:
          curx = int(row['CenterX']); cury = int(row['CenterY'])
          image = cv2.line(image,(prevx,prevy),
                          (curx,cury),self.palette[bid % BLOB_MAX],2)
          prevx = curx; prevy = cury
        else:
          prevx = int(row['CenterX']); prevy = int(row['CenterY'])

    return cv2.cvtColor(image,cv2.COLOR_RGB2BGR)

  def trackBlobs(self):
    """ Function to use detected blobs (pandas dataframe) to update tracking information."""
    self.frame += 1

    if len(self.detections.index) == 0:
      self.logger.warning('No detection plugins enabled!')
      return

    lastFrame = max(self.detections['Frame'])
    lastDetections = self.detections[self.detections['Frame'] == lastFrame]
    if len(self.blobTracks.index) == 0:
      # First frame
      self.blobTracks = pd.concat([self.blobTracks,lastDetections])
      self.blobTracks['Frame'].replace(lastFrame,self.frame)
      maxidx = len(lastDetections['BlobNumber'].index)
      self.blobTracks['BlobID'][0:maxidx] = lastDetections['BlobNumber'][0:maxidx]
    else:
      # Get last detections.  Use Hungarian to minimize cost function, which is the
      # Euclidean distance between previous matches and current detections.
      #pyqtRemoveInputHook()
      #from pudb import set_trace; set_trace()

      lastID = self.blobTracks[self.blobTracks['Frame'] == self.frame-1]
      lastID = lastID.reset_index(drop=True)
      newDetections = self.detections[self.detections['Frame'] == self.frame]
      newDetections = newDetections.reset_index(drop=True)
      totalNew = len(newDetections.index)
      totalOld = len(lastID.index)
      newDetections['BlobID'] = np.full(totalNew,np.nan)
      newDetections['PrevX'] = np.full(totalNew,np.nan)
      newDetections['PrevY'] = np.full(totalNew,np.nan)

      # There are four cases to handle.
      # 1. len(newDetections) == len(lastID).  No blobs have appeared, disappeared,
      # merged, or split. With low probability, a merge or split may have
      # happened simultaneously.
      # 2. len(newDetections) > len(lastID).  Blobs have split and/or appeared.
      #    With very low probability, n splits and n-k merges occurred (k = len(nd)-len(lID))
      # 3. len(newDetections) < len(lastID).  Blobs have merged and/or disappeared.
      #    With very low probability, n merges and n-k splits occurred.

      lastIDx = lastID['CenterX']; lastIDy = lastID['CenterY']
      newX = newDetections['CenterX']; newY = newDetections['CenterY']
      distX = np.subtract.outer(lastIDx,newX)
      distY = np.subtract.outer(lastIDy,newY)
      distMat = np.hypot(distX,distY)

      if totalNew > totalOld:
        # Something showed up. Need a dummy column.
        dummy = np.full((totalNew-totalOld,distMat.shape[1]),distMat.max())
        distMat = np.vstack((distMat,dummy))

      # plus one because the blobs are 1-indexed
      newIDs = linearAssignment(distMat)

      i = int(self.blobTracks['BlobID'].max())+1
      for newid in newIDs:
        row = newid[0]
        col = newid[1]
        try:
          newDetections['BlobID'][col] = lastID['BlobID'][row]
          newDetections['PrevX'][col] = lastID['CenterX'][row]
          newDetections['PrevY'][col] = lastID['CenterY'][row]
        except KeyError:
          newDetections['BlobID'][col] = i
          newDetections['PrevX'][col] = np.nan
          newDetections['PrevY'][col] = np.nan
          i += 1

      self.blobTracks = pd.concat([self.blobTracks,newDetections],ignore_index=True)
      self.parent.recordLabelstack(self.labelstack)
      self.parent.recordTracks(newDetections)

  def options(self):
    # Optional method that returns a QDialog object.  See, e.g. adaptiveMedianBGS.py for an example.
    pass
