# This is a hack that I need to fix.    This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IPreprocessingPlugin
import logging,cv2
import numpy as np
import ConfigParser


class averageBackground(IPreprocessingPlugin):
    """
    Create an average background image from frames across a video, to create a target for moving object detection
    """
    def __init__(self,parent=None,settings=None):
        """
        Constructor for the plugin 
        """
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.background = None
        self.settings = settings
        self.loadSettings()

    def loadSettings(self):
        """
        Load settings for the plugin
        """
        if self.settings:
            self.sampleCount = int(self.settings.get('averageBackground','samplecount',20))
        else:
            raise ValueError('Settings not found.')


    def activate(self):
        super(averageBackground,self).activate()

    def deactivate(self):
        super(averageBackground,self).deactivate()

    def startVideo(self,video):
        from averageWorkerThread import averageWorkerThread

        self.logger.info("averageBackgroundPlugin beginning video processing thread.")
        self.logger.info("averageBackgroundPlugin using samplecount: %d" % self.sampleCount) 
        self.background = None
        self.thread = averageWorkerThread(video,self.sampleCount)
        self.thread.backgroundDone.connect(self.backgroundReady)
        return self.thread

    def startVideoCL(self,videoFile):
        self.logger = logging.getLogger("CLI")
        self.logger.info("averageBackgroundPlugin using samplecount: %d" % self.sampleCount) 
        cap = cv2.VideoCapture(videoFile)
        _,frame1 = cap.read()
        backsum = np.float32(frame1)
        self.frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        if self.frameCount != self.frameCount:
            # WARNING.    This happens when cv2.CAP_PROP_FRAME_COUNT) returns nan.    It's probably a dangerous assumption,
            # but will work well enough for this purpose.
            self.frameCount = 108000

        interval = self.frameCount / self.sampleCount
        tvals = range(0,self.frameCount,int(interval))
        for t in tvals:
            cap.set(cv2.CAP_PROP_POS_MSEC,t)
            _,frame = cap.read()
            cv2.accumulate(frame,backsum)
            self.logger.info("Have done %d background processing steps" % t) 
            
        backsum = backsum / len(tvals)
        self.background = cv2.convertScaleAbs(backsum)


    def backgroundReady(self,background):
        self.background = background

    def run(self,image):
        if type(self.background) == type(None):
            return image
        else:
            foreground = cv2.cvtColor(cv2.absdiff(image,self.background),cv2.COLOR_BGR2GRAY)
            _,self.foregroundmask = cv2.threshold(foreground,75,255,cv2.THRESH_BINARY)
            return cv2.cvtColor(self.foregroundmask,cv2.COLOR_GRAY2BGR)

    def options(self):
        """Options dialog for Average Background plugin.

        Average background has one option
            sampleCount: int value that defines the number of samples taken
        """
        from averageBackgroundOptions import Ui_averageBackgroundOptions
        from PyQt5 import QtWidgets,QtGui

        self.dialog = QtWidgets.QDialog()
        self.optionsDialog = Ui_averageBackgroundOptions()
        self.optionsDialog.setupUi(self.dialog)
        self.optionsDialog.sampleEdit.setValidator(QtGui.QIntValidator())

        self.optionsDialog.sampleEdit.setText(str(self.sampleCount))

        self.dialog.accepted.connect(self.dialogAccepted)
        return self.dialog

    def dialogAccepted(self):
        sampleCount = int(self.optionsDialog.sampleEdit.text())
        self.settings.set('averageBackground','sampleCount',str(sampleCount))
        with open(self.settings.file_path,'w') as f:
          self.settings.write(f)
        self.loadSettings()
