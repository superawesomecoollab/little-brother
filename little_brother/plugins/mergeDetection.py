import sys
sys.path.append('./plugins')

# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IPostprocessingPlugin
from core.utils import current_plugins_dir
import logging
import pickle
import cv2
import numpy as np
from dbops import padImages, transformImages, calculateSmax,preprocessImages, getClassifierData

class mergeDetection(IPostprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    self.parent = parent
    IPostprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    # Instance variables can go here.
    self.rawFrame = None;
    self.contourPixels = None;
    self.tracksRecord = None;

  def activate(self):
    super(mergeDetection,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false
    with open(current_plugins_dir() + 'merge_classifier.pkl','r') as f:
      self.clf = pickle.load(f)

    self.trackscolumsn = self.parent.getColumns('tracks')
    self.visualize = False

  def deactivate(self):
    super(mergeDetection,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in
    # RGB.  Use cv2.cvtColor() if necessary.
    if len(self.tracksRecord.index) == 0:
      return image

    lastFrame = max(self.tracksRecord['Frame'])
    lastFrameDF = self.tracksRecord.loc[self.tracksRecord['Frame'] == lastFrame]
    images = preprocessImages(self.contourPixels,self.rawFrame,image)

    smax = self.clf.smax
    paddedImages = padImages(images,smax)
    transformedImages = transformImages(paddedImages,smax)
    # put the data in n_samples, n_features format.
    #classifierData = np.array(transformedImages)
    classifierData = getClassifierData(transformedImages)

    predicted = self.clf.predict(classifierData)
    predicted = [x != 2 for x in predicted]

    if self.visualize:
      for blob,value in enumerate(predicted):
        if value:
          pixels = np.vstack(self.contourPixels[blob]['Pixels'])
          minx = np.min(pixels[0]); maxx = np.max(pixels[0])
          miny = np.min(pixels[1]); maxy = np.max(pixels[1])
          cv2.putText(image,'M'+" "+str((miny+maxy)/2)+" "+str((minx+maxx)/2),((miny+maxy)/2,(minx+maxx)/2),cv2.FONT_HERSHEY_COMPLEX,3,(0,0,255))

    for index, blob in enumerate(self.contourPixels):
      for idx,row in lastFrameDF.iterrows():
        pixels = np.vstack(self.contourPixels[blob]['Pixels'])
        minx = np.min(pixels[1]); maxx = np.max(pixels[1])
        miny = np.min(pixels[0]); maxy = np.max(pixels[0])
        if (minx < row['CenterX'] < maxx) and (miny < row['CenterY'] < maxy):
          lastFrameDF.loc[idx,'Merged'] = predicted[index]


    self.parent.recordTracks(lastFrameDF)

    return image

  def options(self):
    # Optional method that returns a QDialog object.  See, e.g. adaptiveMedianBGS.py for an example.
    pass
