import sqlite3
import cv2
import numpy as np

def flattenImage(img):
  """
  takes in an (m, n, o) numpy array and flattens it
  into an array of shape (1, m * n * o)
  """
  s = img.shape[0] * img.shape[1] * img.shape[2]
  img_wide = img.reshape(1, s)
  return img_wide[0].reshape(1,s)[0]

def calculateSmax(images):
  """Takes a list of image and returns (j,k,3), where j and k
  are the maximum sizes of rows and columns."""
  shapesx = [ x.shape[0] for x in images]
  shapesy = [ x.shape[1] for x in images]
  smaxx = images[np.where(shapesx == np.max(shapesx))[0][0]].shape[0]
  smaxy = images[np.where(shapesy == np.max(shapesy))[0][0]].shape[1]
  return (smaxx,smaxy,3)

def preprocessImages(contourPixels,rawFrame,image):
  images = np.empty((len(contourPixels,)),dtype=object)
  i = 0
  for blob in contourPixels:
      pixels = np.vstack(contourPixels[blob]['Pixels'])
      minx = np.min(pixels[0]); maxx = np.max(pixels[0])
      miny = np.min(pixels[1]); maxy = np.max(pixels[1])
      #mask = image[minx:maxx,miny:maxy]
      #mask = rawFrame[minx:maxx,miny:maxy]
      blobimage = rawFrame[minx:maxx,miny:maxy]
      # Do I need this line?
      #blobimage = cv2.bitwise_and(blobimage,mask)
      images[i] = blobimage
      i += 1

  images = np.array(images)
  #blobs = np.arange(len(images))

  return images


def getClassifierData(transformedImages):
  classifierData = transformedImages
  n_samples = classifierData.shape[0]
  n_features = classifierData.shape[1]*classifierData.shape[2]*classifierData.shape[3]
  classifierData = classifierData.reshape(n_samples,n_features)
  return classifierData


def padImages(imageList,smax):
  paddedImages = np.empty(shape=(len(imageList),)+smax,dtype=np.uint8)
  for i in range(len(imageList)):
    si = imageList[i].shape;

    left = int(np.ceil((smax[1]-si[1])/2))
    right = smax[1]-si[1]-left
    top = int(np.ceil((smax[0]-si[0])/2))
    bottom = smax[0]-si[0]-top

    if top < 0 or left < 0:
      imageList[i] = cv2.resize(imageList[i],(smax[0],smax[1]))

    else:
      paddedImages[i,...] = cv2.copyMakeBorder(imageList[i],top,bottom,left,right,cv2.BORDER_CONSTANT,(0,0,0))

  return paddedImages

def transformImages(paddedImages,smax):
  transformedImages = np.empty(shape=(len(paddedImages),)+smax,dtype=np.uint8)
  for i in range(len(paddedImages)):
    grey = cv2.cvtColor(paddedImages[i],cv2.COLOR_BGR2GRAY)
    (_,contours,hierarchy) = cv2.findContours(grey,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    try:
      ellipse = [ cv2.fitEllipse(x) for x in contours if len(x) > 5][0]
      centerx = int(ellipse[0][0]); centery = int(ellipse[0][1])
      semix = int(ellipse[1][0]/2); semiy = int(ellipse[1][1]/2)
      angle = ellipse[2]
      area = np.pi * semix * semiy

      rotation = cv2.getRotationMatrix2D((centerx,centery),angle,1)
      im = cv2.warpAffine(paddedImages[i],rotation,(smax[1],smax[0]))
      #transformedImages.append(im)
      transformedImages[i,...] = im
    except IndexError:
      transformedImages[i,...] = paddedImages[i]
  return transformedImages
