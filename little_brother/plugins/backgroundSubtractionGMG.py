from core.plugin_base_classes import IPreprocessingPlugin
import cv2
import logging

class backgroundSubtractGMG(IPreprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    IPreprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")

  def activate(self):
    super(backgroundSubtractGMG,self).activate()
    try:
      self.fgbg = cv2.createBackgroundSubtractorGMG()
    except AttributeError:
      self.logger.warning("backgroundSubtractorGMG requires openCV 3 and the opencv_contrib modules;  see https://github.com/Itseez/opencv_contrib.")
      self.fgbg = None
      self.is_activated = False

  def deactivate(self):
    super(backgroundSubtractGMG,self).deactivate()

  def run(self,image):
    """ Subtracts backgrounds using openCV's GMG based subtractor.

    Args:
      image: An openCV BGR color image.

    Returns:
      An openCV BGR color image.
    """
    if self.fgbg:
      fgmask = self.fgbg.apply(image)
      image = cv2.bitwise_and(image,image,mask=fgmask)
    return image

  def options(self):
    pass
