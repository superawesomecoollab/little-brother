# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plugins/segmentationBackgroundOptions.ui'
#
# Created: Mon May  4 15:26:06 2015
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_segmentationBackgroundOptions(object):
    def setupUi(self, segmentationBackgroundOptions):
        segmentationBackgroundOptions.setObjectName("segmentationBackgroundOptions")
        segmentationBackgroundOptions.resize(370, 155)
        self.buttonBox = QtWidgets.QDialogButtonBox(segmentationBackgroundOptions)
        self.buttonBox.setGeometry(QtCore.QRect(20, 110, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.scaleLabel = QtWidgets.QLabel(segmentationBackgroundOptions)
        self.scaleLabel.setGeometry(QtCore.QRect(40, 20, 62, 16))
        self.scaleLabel.setObjectName("scaleLabel")
        self.sigmaLabel = QtWidgets.QLabel(segmentationBackgroundOptions)
        self.sigmaLabel.setGeometry(QtCore.QRect(40, 50, 62, 16))
        self.sigmaLabel.setObjectName("sigmaLabel")
        self.minSizeLabel = QtWidgets.QLabel(segmentationBackgroundOptions)
        self.minSizeLabel.setGeometry(QtCore.QRect(40, 80, 91, 16))
        self.minSizeLabel.setObjectName("minSizeLabel")
        self.scaleEdit = QtWidgets.QLineEdit(segmentationBackgroundOptions)
        self.scaleEdit.setGeometry(QtCore.QRect(150, 20, 211, 21))
        self.scaleEdit.setObjectName("scaleEdit")
        self.sigmaEdit = QtWidgets.QLineEdit(segmentationBackgroundOptions)
        self.sigmaEdit.setGeometry(QtCore.QRect(150, 50, 211, 21))
        self.sigmaEdit.setObjectName("sigmaEdit")
        self.minSizeEdit = QtWidgets.QLineEdit(segmentationBackgroundOptions)
        self.minSizeEdit.setGeometry(QtCore.QRect(150, 80, 211, 21))
        self.minSizeEdit.setObjectName("minSizeEdit")

        self.retranslateUi(segmentationBackgroundOptions)
        self.buttonBox.accepted.connect(segmentationBackgroundOptions.accept)
        self.buttonBox.rejected.connect(segmentationBackgroundOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(segmentationBackgroundOptions)

    def retranslateUi(self, segmentationBackgroundOptions):
        _translate = QtCore.QCoreApplication.translate
        segmentationBackgroundOptions.setWindowTitle(_translate("segmentationBackgroundOptions", "Dialog"))
        self.scaleLabel.setText(_translate("segmentationBackgroundOptions", "Scale"))
        self.sigmaLabel.setText(_translate("segmentationBackgroundOptions", "Sigma"))
        self.minSizeLabel.setText(_translate("segmentationBackgroundOptions", "Minimum size"))

