# See PluginBaseClasses.py for the categories of plugin.
from core.plugin_base_classes import IAssignmentPlugin
import logging,colorsys,cv2,ConfigParser
import pandas as pd
import numpy as np
import scipy.spatial
from graph import graphAssignment


PREVLINES=20
BLOB_MAX= 10


# Based off of http://stackoverflow.com/questions/876853/generating-color-ranges-in-python
def _get_colors(num_colors):
    HSV_tuples = [(x*1.0/num_colors, 0.5, 0.5) for x in range(num_colors)]
    RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)
    RGB_tuples = map(lambda x: (int(x[0]*255),int(x[1]*255),int(x[2]*255)),RGB_tuples)
    return RGB_tuples

def outputState(df1,df2,labelstack):
  df1.to_pickle('Frame1')
  df2.to_pickle('Frame2')
  labelstack.to_pickle('labelstack')

class graphTrack(IAssignmentPlugin):
  def __init__(self,parent=None,settings=None):
    self.parent = parent
    IAssignmentPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    self.palette = _get_colors(BLOB_MAX) # tune this based on possible number of blobs
    # Instance variables can go here.

  def newDF(self,type):
    return pd.DataFrame(columns=self.parent.getColumns(dfType=type))

  def activate(self):
    super(graphTrack,self).activate()
    # Activation code here, e.g. creation of background subtraction model.
    # If activation fails, set the instance variable .is_activated to false
    self.frame = 0
    self.blobTracks, self.labelstack = (self.newDF("tracks"),self.newDF("labels"))

    self.loadSettings()
    self.assign = graphAssignment(self.areaThreshold,self.distanceThreshold)
    self.detections = None

  def loadSettings(self):
    if self.settings:
      self.areaThreshold = int(self.settings.get('graphTrack','areaThreshold',1100))
      self.distanceThreshold = int(self.settings.get('graphTrack','distanceThreshold',60))
      self.decayThreshold = int(self.settings.get('graphTrack','decayThreshold',1000))
      self.drawBlobLabels = self.settings.getboolean('graphTrack','blobLabels')
    else:
      raise ValueError('Settings not found.')
      

  def deactivate(self):
    super(graphTrack,self).deactivate()
    # Cleanup code goes here.

  def run(self,image):
    # Processing code goes here.
    # run must return a BGR-encoded openCV image.  Remember that openCV does not encode in
    # RGB.  Use cv2.cvtColor() if necessary.
    self.trackBlobs()

    if self.drawBlobLabels:
      image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
      binary = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
      _,binary = cv2.threshold(binary,10,255,cv2.THRESH_BINARY)
      binaryrgb = cv2.cvtColor(binary,cv2.COLOR_GRAY2RGB)
      ids = self.blobTracks[self.blobTracks['Frame'] == self.frame]
      ids = ids.dropna(subset=['BlobID'])
      if ids.empty:
        return cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
      h, w = binary.shape[:2]
      mask = np.zeros((h+2, w+2), np.uint8)
      mask[:] = 0

      maskpoints = np.transpose(np.nonzero(binary))
      maskpoints[:,[0,1]] = maskpoints[:,[1,0]]

      # # use a spatial k-d tree to speed up finding nearest coloured pixel mass.
      # # this is all necessary because using the ellipse centroid can miss
      # # the mass of the irregularly shaped blob contours.
      tree = scipy.spatial.KDTree(maskpoints)
      for count, row in ids.iterrows():
        centerx = int(row['CenterX']); centery = int(row['CenterY'])
        dist,indexes = tree.query((centerx,centery))
        if not np.isinf(dist):
          nearestx,nearesty = maskpoints[indexes]
        # colour modulo BLOB_MAX for when new blob ids exceed the number of colours
        (_,image,_,rect) = cv2.floodFill(binaryrgb,mask,(nearestx,nearesty),self.palette[int(row['BlobID']) % BLOB_MAX])
        image = cv2.circle(image,(centerx,centery),3,(255,0,0),-1)
        cv2.putText(image,str(int(row['BlobID'])),
          (centerx,centery),cv2.FONT_HERSHEY_TRIPLEX,3,255)

      # Draw lines for PREVLINES iterations
      for blobid in ids['BlobID']:
        bid = int(blobid)
        if bid != -1:
          prevx = None; prevy = None
          for count,row in self.blobTracks[self.blobTracks['BlobID']==bid].tail(PREVLINES).iterrows():
            if prevx and prevy:
              curx = int(row['CenterX']); cury = int(row['CenterY'])
              image = cv2.line(image,(prevx,prevy),
                              (curx,cury),self.palette[bid % BLOB_MAX],2)
              prevx = curx; prevy = cury
            else:
              prevx = int(row['CenterX']); prevy = int(row['CenterY'])

      return cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
    else:
      return image

  def trackBlobs(self):
    """ Function to use detected blobs (pandas dataframe) to update tracking information."""
    self.frame += 1

    if len(self.detections.index) == 0:
      #self.logger.warning('No detection plugins enabled!')
      return

    # Assume that if there's a massive blob, something is wrong and skip the frame
    if (self.detections['Area'] > 20000).any(0):
      self.parent.recordLabelstack(self.labelstack)
      self.parent.recordTracks(self.blobTracks)
      return

    lastFrame = max(self.detections['Frame'])
    lastDetections = self.detections[self.detections['Frame'] == lastFrame]
    if len(self.blobTracks.index) == 0:
      # First frame
      self.blobTracks = pd.concat([self.blobTracks,lastDetections])
      self.blobTracks['Frame'].replace(lastFrame,self.frame)
      maxidx = len(lastDetections['BlobNumber'].index)
      self.blobTracks['BlobID'][0:maxidx] = lastDetections['BlobNumber'][0:maxidx]

      # If there's any already-existing merged blobs, push them on the stack for
      # the case when they split and reform.
      self.blobTracks.loc[self.blobTracks['Area'] > self.areaThreshold,'BlobID'] = -1
      return (self.blobTracks,self.labelstack)
    else:
      # Get last detections.  Use Hungarian to minimize cost function, which is the
      # Euclidean distance between previous matches and current detections.
      lastID = self.blobTracks[self.blobTracks['Frame'] == self.frame-1]
      lastID = lastID.reset_index(drop=True)
      newDetections = self.detections[self.detections['Frame'] == self.frame]
      newDetections = newDetections.reset_index(drop=True)
      totalNew = len(newDetections.index)
      totalOld = len(lastID.index)

      if totalNew == 0:
        # The last blob is gone.  We can return safely.
        return

      newDetections['BlobID'] = np.full(totalNew,np.nan)
      newDetections['PrevX'] = np.full(totalNew,np.nan)
      newDetections['PrevY'] = np.full(totalNew,np.nan)

      df1,df2,self.labelstack = self.assign.track(lastID,newDetections,self.labelstack,self.parent.nextID())

      # Sanity checking
      assert -1 not in self.labelstack['BlobID'], "-1 on stack in frame %d" % self.frame
      #assert not np.isnan(df2['BlobID'].values).any(), "nan in result in frame %d" % self.frame

      # Add current frame to labelstack
      #self.labelstack['CurrentFrame'].fillna(value=self.frame,inplace=True)
      self.labelstack['CurrentFrame'] = self.frame

      # Drop labels that are too old to avoid matching them later.
      ages = (self.frame - self.labelstack['Frame'].values) > self.decayThreshold
      self.labelstack = self.labelstack.drop(self.labelstack.index[ages])
      self.labelstack = self.labelstack.reset_index(drop=True)
      self.blobTracks = pd.concat([self.blobTracks,df2],ignore_index=True)

      self.parent.recordLabelstack(self.labelstack)
      self.parent.recordTracks(df2)


  def options(self):
    """ Options for the graph tracking plugin.

    Graph tracking has four options:
      areaThreshold: in pixels, defines the area of merged blobs.
      distanceThreshold: in pixels, defines the cutoff for graph-based merging/splitting
      decayThreshold: in frames, the number of frames after which old IDs are discarded from the labelstsack
      drawLabels: boolean, whether to draw the labels on the blobs.
    """
    from graphTrackOptions import Ui_GraphTrackOptions
    from PyQt5 import QtWidgets,QtGui

    self.dialog = QtWidgets.QDialog()
    self.optionsDialog = Ui_GraphTrackOptions()
    self.optionsDialog.setupUi(self.dialog)

    self.optionsDialog.areaThresholdEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.distanceThresholdEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.decayThresholdEdit.setValidator(QtGui.QIntValidator())

    self.optionsDialog.areaThresholdEdit.setText(str(self.settings.get('graphTrack','areaThreshold')))
    self.optionsDialog.distanceThresholdEdit.setText(str(self.settings.get('graphTrack','distanceThreshold')))
    self.optionsDialog.decayThresholdEdit.setText(str(self.settings.get('graphTrack','decayThreshold')))
    self.optionsDialog.blobLabelBox.setChecked(self.settings.getboolean('graphTrack','blobLabels'))

    self.dialog.accepted.connect(self.dialogAccepted)
    return self.dialog

  def dialogAccepted(self):
    self.settings.set('graphTrack','areaThreshold',str(int(self.optionsDialog.areaThresholdEdit.text())))
    self.settings.set('graphTrack','distanceThreshold',str(int(self.optionsDialog.distanceThresholdEdit.text())))
    self.settings.set('graphTrack','decayThreshold',str(int(self.optionsDialog.decayThresholdEdit.text())))
    self.settings.set('graphTrack','blobLabels',str(self.optionsDialog.blobLabelBox.isChecked()))

    with open(self.settings.file_path,'w') as f:
      self.settings.write(f)

    self.loadSettings()
