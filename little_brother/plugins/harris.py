# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IPreprocessingPlugin
import cv2,logging
import numpy as np
import ConfigParser

class Harris(IPreprocessingPlugin):
    def __init__(self,parent=None,settings=None):
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.settings = settings
        self.loadSettings()

    def loadSettings(self):
        if self.settings:
            self.blocksize = int(self.settings.get('harris','blocksize',5))
            self.ksize = int(self.settings.get('harris','ksize',5))
            self.k = float(self.settings.get('harris','k',0.04))
        else:
            raise ValueError('Settings not found.')


    def activate(self):
        super(Harris,self).activate()

    def deactivate(self):
        super(Harris,self).deactivate()

    def run(self,image):
        """ Detects and plots Harris corners.

        Args:
          image: An opencv image

        Returns:
          A opencv image marked with corners in red.
        """

        gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        gray = np.float32(gray)
        dst = cv2.cornerHarris(gray,self.blocksize,self.ksize,self.k)
        dst = cv2.dilate(dst,None)
        image[dst>0.01*dst.max()]=[0,0,255]
        return image

    def options(self):
        """Options dialog for the Harris plugin.

        The Harris plugin accepts three options (see http://docs.opencv.org/trunk/doc/py_tutorials/py_feature2d/py_features_harris/py_features_harris.html):
          blocksize: the size of the neighborhood for corner detection.
          ksize: aperture parameter of the Sobel derivative.
          k: Harris detector free parameter
          """
        from harrisOptions import Ui_harrisOptions
        from PyQt5 import QtWidgets,QtGui

        self.dialog = QtWidgets.QDialog()
        self.optionsDialog = Ui_harrisOptions()
        self.optionsDialog.setupUi(self.dialog)

        self.optionsDialog.blocksizeEdit.setValidator(QtGui.QIntValidator())
        self.optionsDialog.ksizeEdit.setValidator(QtGui.QIntValidator())
        self.optionsDialog.kEdit.setValidator(QtGui.QDoubleValidator())

        self.optionsDialog.blocksizeEdit.setText(str(int(self.settings.get('harris','blocksize'))))
        self.optionsDialog.ksizeEdit.setText(str(int(self.settings.get('harris','ksize'))))
        self.optionsDialog.kEdit.setText(str(float(self.settings.get('harris','k'))))

        self.dialog.accepted.connect(self.dialogAccepted)
        return self.dialog

    def dialogAccepted(self):
        self.settings.set('harris','blocksize',str(int(self.optionsDialog.blocksizeEdit.text())))
        self.settings.set('harris','ksize',str(int(self.optionsDialog.ksizeEdit.text())))
        self.settings.set('harris','k',str(float(self.optionsDialog.kEdit.text())))

        with open(self.settings.file_path,'w') as f:
          self.settings.write(f)


        self.loadSettings()
