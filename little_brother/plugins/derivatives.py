# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IPreprocessingPlugin
import cv2
import numpy as np
import logging
import ConfigParser


class Derivatives(IPreprocessingPlugin):
    def __init__(self,parent=None,settings=None):
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.settings = settings
        self.loadSettings()

    def loadSettings(self):
        if self.settings:
            self.kernelSize = int(self.settings.get('derivatives','kernelSize',3))
            self.xorder = int(self.settings.get('derivatives','xorder',1))
            self.yorder = int(self.settings.get('derivatives','yorder',0))
            self.xdirection = self.settings.getboolean('derivatives','xdirection')
            self.ydirection = self.settings.getboolean('derivatives','ydirection')
        else:
            raise ValueError('Settings not found.')


    def activate(self):
      super(Derivatives,self).activate()

    def deactivate(self):
      super(Derivatives,self).deactivate()

    def run(self,image):
        """ Computes the Sobel derivatives of an image

        Args:
          image: An opencv image

        Returns:
          An opencv image.
        """
        # This conversion back and forth is required because
        # QImage doesn't have a native 8-bit grayscale color model
        # and so we need to bring it back to RGB via BGR.
        newframe = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        newframe_x = cv2.Sobel(newframe, cv2.CV_16S, self.xorder,0,ksize=(2*self.kernelSize)+1,borderType=cv2.BORDER_DEFAULT)
        newframe_y = cv2.Sobel(newframe, cv2.CV_16S, 0,self.yorder,ksize=(2*self.kernelSize)+1,borderType=cv2.BORDER_DEFAULT)

        absnewframe_x = cv2.convertScaleAbs(newframe_x)
        absnewframe_y = cv2.convertScaleAbs(newframe_y)

        if self.xdirection and self.ydirection:
          newframe = cv2.addWeighted(absnewframe_x,0.5,absnewframe_y,0.5,0)
        else:
          newframe = absnewframe_x if self.xdirection else absnewframe_y

        newframe = cv2.cvtColor(newframe,cv2.COLOR_GRAY2BGR)
        return newframe

    def options(self):
        """Kernel options dialog.

          Options for the kernel.
            element: 0,1,2 = Rect, Cross, Ellipse.
            Kernel size: 2n+1 from 0 to 12
        """
        from derivativeOptions import Ui_derivativeOptions
        from PyQt5 import QtWidgets,QtGui

        self.dialog = QtWidgets.QDialog()
        self.optionsDialog = Ui_derivativeOptions()
        self.optionsDialog.setupUi(self.dialog)

        self.optionsDialog.kernelSlider.setValue(int(self.settings.get('derivatives','kernelSize',3)))
        self.optionsDialog.xCheckBox.setChecked(self.settings.getboolean('derivatives','xdirection'))
        self.optionsDialog.yCheckBox.setChecked(self.settings.getboolean('derivatives','ydirection'))

        self.dialog.accepted.connect(self.dialogAccepted)
        return self.dialog

    def dialogAccepted(self):
        self.settings.set('derivatives','kernelSize',str(int(self.optionsDialog.kernelSlider.value())))
        self.settings.set('derivatives','xdirection',str(self.optionsDialog.xCheckBox.isChecked()))
        self.settings.set('derivatives','ydirection',str(self.optionsDialog.yCheckBox.isChecked()))

        with open(self.settings.file_path,'w') as f:
          self.settings.write(f)

        self.loadSettings()
