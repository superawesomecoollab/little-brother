import cv2
import numpy as np
import logging
from PyQt5.QtCore import *    

from adaptiveBackground import calcThresholds

class adaptiveWorkerThread(QThread):
    """
    Worker class for adaptive Background Detection
    """
    backgroundDone = pyqtSignal(object, object)

    def __init__(self,video,sampleCount=20):
        """
        Constructor for the background subtraction worker class
        
        @param video : the loaded video to be processed.
        @param sampleCount : default will be 20 images to create the background average.
        """
        QThread.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.video = video
        self.background = None
        self.frameCount = int(self.video.getFrameCount())
        self.logger.debug("Video has %d frames to process." % self.frameCount)
        self.frameCounter = 0
        if sampleCount < 1:
            sampleCount = 1
        if sampleCount > self.frameCount:
            sampleCount = self.frameCount    


        # Load a copy to avoid race conditions between the work and main threads.
        # I don't like it, because it will slow things down, but it's a necessary guard.
        self.cap = cv2.VideoCapture(self.video.filename)
        _,frame1 = self.cap.read()
        self.backsum = np.float32(frame1)

        self.interval = int(self.frameCount) / int(sampleCount)
        self.logger.debug("Interval: %d frames" % self.interval)

        # Sample every however many frames
        self.t = range(0,self.frameCount,int(self.interval))


    def doWork(self):
        """
        Adds each subsequent frame to a data frame, in order to construct the average
        """

        try:
            t = self.t.pop(0)

            try:
                self.cap.set(cv2.CAP_PROP_POS_MSEC,t)
            except AttributeError:
                self.cap.set(cv2.cv.CV_CAP_PROP_POS_MSEC,t)

            _,frame = self.cap.read()
            if type(frame) == type(None):
                raise ValueError
            cv2.accumulate(frame, self.backsum)

            self.frameCounter += 1

            self.logger.debug("Adaptive Background processed %d frames" % self.frameCounter)
        except IndexError,ValueError:
            # When the list is empty, it will throw IndexError
            self.backsum = self.backsum / self.frameCounter
            self.background = cv2.convertScaleAbs(self.backsum)
            self.thresholds = calcThresholds(self.background)
            self.backgroundDone.emit(self.background, self.thresholds)
            self.exit()
