from core.plugin_base_classes import IPreprocessingPlugin
import cv2,logging

class Greyscale(IPreprocessingPlugin):
    def __init__(self,parent=None,settings=None):
        IPreprocessingPlugin.__init__(self)
        self.logger = logging.getLogger("Gui")
        self.settings = settings

    def activate(self):
      super(Greyscale,self).activate()

    def deactivate(self):
      super(Greyscale,self).deactivate()

    def run(self,image):
        """ Converts an image to grayscale.

        Args:
          image: An opencv image

        Returns:
          A grayscale opencv image.
        """
        # This conversion back and forth is required because
        # QImage doesn't have a native 8-bit grayscale color model
        # and so we need to bring it back to RGB via BGR.
        newframe = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        newframe = cv2.cvtColor(newframe,cv2.COLOR_GRAY2BGR)
        return newframe
