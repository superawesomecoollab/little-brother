# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'harrisOptions.ui'
#
# Created: Wed Jun 17 17:03:42 2015
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_harrisOptions(object):
    def setupUi(self, harrisOptions):
        harrisOptions.setObjectName("harrisOptions")
        harrisOptions.resize(286, 199)
        self.buttonBox = QtWidgets.QDialogButtonBox(harrisOptions)
        self.buttonBox.setGeometry(QtCore.QRect(60, 140, 181, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.blocksizeEdit = QtWidgets.QLineEdit(harrisOptions)
        self.blocksizeEdit.setGeometry(QtCore.QRect(110, 30, 121, 21))
        self.blocksizeEdit.setObjectName("blocksizeEdit")
        self.blocksizeLabel = QtWidgets.QLabel(harrisOptions)
        self.blocksizeLabel.setGeometry(QtCore.QRect(30, 26, 61, 20))
        self.blocksizeLabel.setToolTip("")
        self.blocksizeLabel.setObjectName("blocksizeLabel")
        self.ksizeLabel = QtWidgets.QLabel(harrisOptions)
        self.ksizeLabel.setGeometry(QtCore.QRect(30, 60, 62, 16))
        self.ksizeLabel.setToolTip("")
        self.ksizeLabel.setObjectName("ksizeLabel")
        self.kLabel = QtWidgets.QLabel(harrisOptions)
        self.kLabel.setGeometry(QtCore.QRect(30, 90, 21, 16))
        self.kLabel.setToolTip("")
        self.kLabel.setObjectName("kLabel")
        self.ksizeEdit = QtWidgets.QLineEdit(harrisOptions)
        self.ksizeEdit.setGeometry(QtCore.QRect(110, 60, 121, 21))
        self.ksizeEdit.setObjectName("ksizeEdit")
        self.kEdit = QtWidgets.QLineEdit(harrisOptions)
        self.kEdit.setGeometry(QtCore.QRect(110, 90, 121, 21))
        self.kEdit.setObjectName("kEdit")

        self.retranslateUi(harrisOptions)
        self.buttonBox.accepted.connect(harrisOptions.accept)
        self.buttonBox.rejected.connect(harrisOptions.reject)
        QtCore.QMetaObject.connectSlotsByName(harrisOptions)

    def retranslateUi(self, harrisOptions):
        _translate = QtCore.QCoreApplication.translate
        harrisOptions.setWindowTitle(_translate("harrisOptions", "harrisOptions"))
        self.blocksizeLabel.setText(_translate("harrisOptions", "Blocksize"))
        self.ksizeLabel.setText(_translate("harrisOptions", "ksize"))
        self.kLabel.setText(_translate("harrisOptions", "k"))

