# This is a hack that I need to fix.  This module is imported one level up by yapsy,
# which means that when it goes to import its Ui, it can't find it.
import sys
sys.path.append('./plugins')

from core.plugin_base_classes import IPreprocessingPlugin
import numpy as np
import cv2
import logging
import ConfigParser

# try:
#   from PyQt5.QtCore import *
# except:
#   pass

BACKGROUND=0
FOREGROUND=255

class adaptiveMedianBGS(IPreprocessingPlugin):
  def __init__(self,parent=None,settings=None):
    IPreprocessingPlugin.__init__(self)
    self.logger = logging.getLogger("Gui")
    self.settings = settings
    self.loadSettings()

  def loadSettings(self):
    if self.settings:
      self.lowThreshold = int(self.settings.get('adaptiveMedianBGS','lowThreshold',15))
      self.highThreshold = int(self.settings.get('adaptiveMedianBGS','highThreshold',25))
      self.sampleRate = int(self.settings.get('adaptiveMedianBGS','sampleRate',15))
      self.learningFrames = int(self.settings.get('adaptiveMedianBGS','learningFrames',30))
    else:
      raise ValueError('Settings not found.')


  def activate(self):
    super(adaptiveMedianBGS,self).activate()
    self.medianImage = None;
    self.initalized = False;

  def decative(self):
    super(adaptiveMedianBGS,self).deactivate()

  def subtract(self,image):
    """Subtract the current frame from the background model and produce a binary
    foreground mask using the low and high threshold value."""

    # Note that numpy absolute causes a huge issue by - for some reason -
    # thresholding the difference somehow.  DO NOT USE.
    #diff = np.absolute(image-self.medianImage)
    diff = cv2.absdiff(image,self.medianImage)

    self.lowThresholdMask = 255-cv2.inRange(diff,np.full(3,0),np.full(3,self.lowThreshold))
    self.highThresholdMask = 255-cv2.inRange(diff,np.full(3,0),np.full(3,self.highThreshold))

  def update(self,image):
    """Update the background model.  Only pixels set to background in self.updateMask are updated."""
    if self.frameNum % self.sampleRate == 1 or self.frameNum < self.learningFrames:
      self.medianImage[image > self.medianImage] += 1
      self.medianImage[image < self.medianImage] -= 1

  def run(self,image):
    """ Subtracts backgrounds using an Adaptive Median BGS.

    Args:
      image: An openCV BGR color image.

    Returns:
      An openCV BGR color image.
    """
    if not self.initalized:
      self.frameNum = 1
      self.medianImage = image.copy()
      self.lowThresholdMask = np.array(np.full((image.shape[0],image.shape[1]),BACKGROUND),np.uint8)
      self.highThresholdMask = np.array(np.full((image.shape[0],image.shape[1]),BACKGROUND),np.uint8)
      self.initalized = True

    self.subtract(image)

    # Delete this if it turns out to be unnecessary.
    self.lowThresholdMask = np.zeros((image.shape[0],image.shape[1]),np.uint8)
    self.update(image)

    self.frameNum += 1
    return cv2.cvtColor(self.highThresholdMask,cv2.COLOR_GRAY2BGR)

  def options(self):
    """Options dialog for the Adaptive Median BGS plugin.

    AdaptiveMedianBGS plugin has four options:
      lowThreshold:  difference value between median and current image that defines background pixels.
      highTreshold:  difference value between median and current image that defines foreground pixels.
      sampleRate:    how many frames apart the median image is updated.
      learningFrames: initial number of frames to construct the background model with.
      """

    from adaptiveMedianBGSOptions import Ui_AdaptiveMedianBGSOptions
    from PyQt5 import QtWidgets,QtGui
      
    self.dialog = QtWidgets.QDialog()
    self.optionsDialog = Ui_AdaptiveMedianBGSOptions()
    self.optionsDialog.setupUi(self.dialog)

    self.optionsDialog.lowEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.highEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.learningFramesEdit.setValidator(QtGui.QIntValidator())
    self.optionsDialog.learningRateEdit.setValidator(QtGui.QIntValidator())

    self.optionsDialog.lowEdit.setText(str(self.lowThreshold))
    self.optionsDialog.highEdit.setText(str(self.highThreshold))
    self.optionsDialog.learningFramesEdit.setText(str(self.sampleRate))
    self.optionsDialog.learningRateEdit.setText(str(self.learningFrames))

    self.dialog.accepted.connect(self.dialogAccepted)
    return self.dialog

  def dialogAccepted(self):
    self.settings.set('adaptiveMedianBGS','lowThreshold',str(int(self.optionsDialog.lowEdit.text())))
    self.settings.set('adaptiveMedianBGS','highThreshold',str(int(self.optionsDialog.highEdit.text())))
    self.settings.set('adaptiveMedianBGS','sampleRate',str(int(self.optionsDialog.learningRateEdit.text())))
    self.settings.set('adaptiveMedianBGS','learningFrames',str(int(self.optionsDialog.learningFramesEdit.text())))

    with open(self.settings.file_path,'w') as f:
      self.settings.write(f)

    self.loadSettings()

    self.initalized = False
