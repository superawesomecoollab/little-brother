import logging,logging.handlers

class QtHandler(logging.Handler):
  """ Handles logging to a QTextEdit.  Inspiration for this came from http://pantburk.info/?blog=77 and https://docs.python.org/2/library/logging.handlers.html#module-logging.handlers

  Attributes:
      textedit: the QTextEdit to which logging output will be emitted.
  """
  def __init__(self,textedit):
    """Inits QtHandler with a QTextEdit to log to."""
    logging.Handler.__init__(self)
    self.textedit = textedit
    self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')
    self.setFormatter(self.formatter)
  def emit(self,record):
    """Emits a logging record to the textedit after formatting it."""
    self.textedit.append(self.format(record))
