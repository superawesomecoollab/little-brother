import pandas as pd
import numpy as np

try:
    from BlobTrackerWidget import BlobTrackerWidget
except:
    pass


class BlobTracker():
    """ Data model class retaining a history of blob detections, tracks and stack labels.

    Note that for long videos the Pandas dataframes used to track the history and label stack can blow up in size and require large amounts of memory. To avoid this issue, both are abbreviated using the history_length  and label_lifespan class attributes.
    """

    history_length = 100  # Note that this is the number of rows;  rows = frames * blobs per frame.
    label_lifespan = 1000 # Also rows, though here it is frames * labels in each frame.

    def __init__(self,cli = False,widget=None):
        """ Constructor for the Blob Tracker.

            :param cli: Boolean flag for the command line interface.
            :param widget: An optional BlobTrackerWidget instance to display values in the GUI.
        """
        self.cli = cli
        self.reset()
        if not cli:
            self.widget = widget
            if self.widget:
                widget.reset(self.blobTracks.columns.values.tolist())

    def reset(self):
        """Reset the blob, track, and label Pandas dataframes.
        """
        self.detections = pd.DataFrame(columns=self.getColumns("detections"))
        self.blobTracks = pd.DataFrame(columns=self.getColumns("tracks"))
        self.labelstack = pd.DataFrame(columns=self.getColumns("tracks"))
        self.detectionPixels = None

    def getColumns(self,dfType = "tracks"):
        """Returns a list of column names for dataframes of either tracks, blobs, or stack labels.

            @throw ValueError: An error occurred determining the type of data frame column names to return.
        """
        if dfType == "tracks":
            return ["Frame","BlobNumber","CenterX","CenterY", "Height","Width","Area","Angle","BlobID","PrevX","PrevY","Merged","Color","Sex", "Heading"]
        elif dfType == "detections":
            return ["Frame","BlobNumber","CenterX","CenterY","Height","Width","Area","Angle"]
        elif dfType == "labels":
            return ["CurrentFrame","Frame","BlobNumber","CenterX","CenterY", "Height","Width","Area","Angle","BlobID","PrevX","PrevY","Merged","Color","Sex", "Heading"]
        else:
            raise ValueError,"Unknown data frame type in getColumns"

    def nextID(self):
        """Returns the next available ID for newly identified blobs. If no blobs have yet been identified, it returns 1.
        """
        try:
            return int(pd.concat([self.blobTracks['BlobID'],self.labelstack['BlobID']]).max())+1
        except:
            return 1

    def recordDetections(self,detectionDF):
        """Returns an updated Pandas dataframe of blob detections.

            :param detectionDF: blobs detected in the current frame.
        """
        self.detections = pd.concat([self.detections,detectionDF])

    def getDetections(self):
        """Returns the dataframe containing all the recorded detections.
        """
        return self.detections

    def recordDetectionPixels(self,pixels):
        """Returns detection pixel data (from plugins with base class IDetectionPlugin).
        """
        self.detectionPixels = pixels

    def getDetectionPixels(self):
        """Returns the dataframe containing the recorded pixel information for all blobs in a frame.
        """
        return self.detectionPixels

    def recordTracks(self,tracksDF):
        """Updates the recorded tracks.

            :param tracksDF: A pandas dataframe of type "tracks" (see getColumns).
        """
        if len(self.blobTracks.index) > 0 and (max(self.blobTracks['Frame']) == max(tracksDF['Frame'])):
            self.blobTracks = self.blobTracks[self.blobTracks['Frame'] != max(tracksDF['Frame'])]
        self.blobTracks = pd.concat([self.blobTracks,tracksDF])

        # Memory requirements grow large over 100K frames. Drop all but the last few frames.
        # This is a hack, and a bit dangerous; if there's more than X blobs on the screen...
        if len(self.blobTracks.index) > self.history_length:
            self.blobTracks = self.blobTracks.tail(50)

        if not self.cli:
            self.widget.update(tracksDF)

    def getTracks(self):
        """Returns the Pandas dataframe containing blob tracking data from previous frames.
        """
        return self.blobTracks

    def recordLabelstack(self,labelstackDF):
        """Updates the label stack with new merged blob labels. 

        :param labelStackDF: a Pandas dataframe of current labels that represent blobs which merged in the last frame.
        """
        if len(self.labelstack.index) > 0 and len(labelstackDF.index) > 0 and (max(self.labelstack['CurrentFrame']) == max(labelstackDF['CurrentFrame'])):
            self.labelstack = self.labelstack[self.labelstack['CurrentFrame'] != max(labelstackDF['CurrentFrame'])]
        self.labelstack = pd.concat([self.labelstack,labelstackDF])

        if len(self.labelstack.index) > self.label_lifespan:
            self.labelstack = self.labelstack.tail(50)

    def getLabelstack(self):
        """Returns a Pandas dataframe containing the current label stack.
        """
        return self.labelstack

    def loadTracks(self,filename):
        """Loads tracks into a BlobTracker instance from a file.  Used mainly for testing.

        :param filename: Filename of the tracks CSV file to load as a string.  Must conform to known columns (see getColumns).
        """
        import os

        if os.path.exists(filename):        
            self.blobTracks = pd.read_csv(filename)            
        
