import cv2,sys,logging
import numpy as np
try:
	from PyQt5 import QtGui, QtCore, Qt
except:
	class QWidget():
		pass
#from ui import Ui_MainWindow

class Video():
	"""Data model class to process the video frame data. 
	"""
	def __init__(self,filename,cli=False):
		"""The constructor for the Video processor.
		 
		:param filename: Name of the .avi to be opened.
		:param cli: Boolean flag for the command line interface.
		"""
		self.logger = logging.getLogger("Gui")
		self.filename = filename
		self.cli = cli
		self.capture = cv2.VideoCapture(self.filename)

		if not self.capture:
			raise ValueError("Could not open %s" % self.filename)

		self.currentFrame = np.array([])
		self.rawFrame = np.array([])
		self.frameNumber = 0


	def captureNextFrame(self):
		"""Method to capture frames from a frame.
		
		:rtype: An opencv image in BGR format.
		"""
		ret, readFrame=self.capture.read()
		if(ret==True):
			#self.currentFrame=cv2.cvtColor(readFrame,cv2.COLOR_BGR2RGB)
			self.currentFrame = readFrame
			self.rawFrame = readFrame
			self.frameNumber += 1
		return ret

	# TODO: refactor filter to plugin
	def runFilter(self,filter,name=None):
		"""Run the supplied plugin on the current frame.

		:param filter: The filter, or plugin, being used.
		:param name: Optional, the name of plugin for debugging purposes.
		"""
		try:
			self.currentFrame = filter.run(self.currentFrame)
		except :
			self.logger.exception("Error in plugin %s (filename: %s):" % (name,self.filename))

	def provideRawFrame(self,filter):
		"""Passes the current raw frame data to a filter.
		"""		
		filter.rawFrame = self.rawFrame

	def provideFrameCount(self,filter):
		"""Passes the current video's total frame count to a filter.
		"""
		filter.frameCount = self.getFrameCount()

	def convertFrame(self):
		"""Converts a frame to a format suitable for QtGui.
		
		:rtype: either an OpenCV image if in GUI mode, or nothing if in command line mode.
		"""
		self.currentFrame = cv2.cvtColor(self.currentFrame,cv2.COLOR_BGR2RGB)
		if not self.cli:
			height,width=self.currentFrame.shape[:2]
			img=QtGui.QImage(self.currentFrame,
							 width,
							 height,
							 QtGui.QImage.Format_RGB888)
			img=QtGui.QPixmap.fromImage(img)
			self.previousFrame = self.currentFrame
			return img
		else:
			return None

	def getCurrentFrame(self):
		"""Returns the current frame of the capture.

		:rtype: The integer frame number of the current video position.
		"""
		try:
			frame = self.capture.get(cv2.CAP_PROP_POS_FRAMES)
		except AttributeError:
			frame = self.capture.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
		return frame

	def getFrameCount(self):
		"""Returns the total frame count of a video.

		:rtype: The integer frame count (the total length of the video in frames).
		"""
		try:
			fc = self.capture.get(cv2.CAP_PROP_FRAME_COUNT)
		except AttributeError:
			fc = self.capture.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
		return fc

	def seekToFrame(self,frame):
		"""Access a frame defined by an index.
		
		:param frame: the index for the desired frame.
		"""
		try:
			self.capture.set(cv2.CAP_PROP_POS_FRAMES,frame)
		except AttributeError:
			self.capture.set(cv2.cv.CAP_PROP_POS_FRAMES,frame)
		finally:
			self.frameNumber = frame
		

