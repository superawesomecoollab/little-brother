import sys
import os
import inspect

def base_dir():
	""" Returns the base directory for path calculation purposes.  Base directory may be different depending on whether
	the program is run as a script at the command line or frozen as an app, for instance."""
	if getattr(sys, 'frozen', False):
		#basedir = os.path.dirname(sys._MEIPASS.rstrip("/"))
		basedir = sys._MEIPASS
	else:
		basedir = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
	return basedir

def current_plugins_dir():
	return base_dir()+"/plugins/"