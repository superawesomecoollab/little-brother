import sys
import os
import imp

from yapsy import log
from yapsy import NormalizePluginNameForModuleName
from yapsy.IPlugin import IPlugin
from yapsy.PluginManagerDecorator import  PluginManagerDecorator

class DelegatePluginManager(PluginManagerDecorator):
  def __init__(self, decorated_manager=None, categories_filter={"Default":IPlugin}, directories_list=None, plugin_info_ext="yapsy-plugin", parent=None,settings=None):
    self.parent = parent
    self.settings = settings
    PluginManagerDecorator.__init__(self,decorated_manager,categories_filter,directories_list,plugin_info_ext)

  def loadPlugins(self, callback=None):
    if not hasattr(self, '_candidates'):
      raise ValueError("locatePlugins must be called before loadPlugins")


    processed_plugins = []
    for candidate_infofile, candidate_filepath, plugin_info in self._candidates:
        # make sure to attribute a unique module name to the one
        # that is about to be loaded
        plugin_module_name_template = NormalizePluginNameForModuleName("yapsy_loaded_plugin_" + plugin_info.name) + "_%d"
        for plugin_name_suffix in range(len(sys.modules)):
            plugin_module_name =  plugin_module_name_template % plugin_name_suffix
            if plugin_module_name not in sys.modules:
                break

        # tolerance on the presence (or not) of the py extensions
        if candidate_filepath.endswith(".py"):
            candidate_filepath = candidate_filepath[:-3]
        # if a callback exists, call it before attempting to load
        # the plugin so that a message can be displayed to the
        # user
        if callback is not None:
            callback(plugin_info)
        # cover the case when the __init__ of a package has been
        # explicitely indicated
        if "__init__" in  os.path.basename(candidate_filepath):
            candidate_filepath = os.path.dirname(candidate_filepath)
        try:
            # use imp to correctly load the plugin as a module
            if os.path.isdir(candidate_filepath):
                candidate_module = imp.load_module(plugin_module_name,None,candidate_filepath,("py","r",imp.PKG_DIRECTORY))
            else:
                plugin_file = open(candidate_filepath+".py","r")
                try:
                    candidate_module = imp.load_module(plugin_module_name,plugin_file,candidate_filepath+".py",("py","r",imp.PY_SOURCE))
                finally:
                    plugin_file.close()
        except Exception:
            exc_info = sys.exc_info()
            log.error("Unable to import plugin: %s" % candidate_filepath, exc_info=exc_info)
            plugin_info.error = exc_info
            processed_plugins.append(plugin_info)
            continue
        processed_plugins.append(plugin_info)

        if "__init__" in  os.path.basename(candidate_filepath):
            sys.path.remove(plugin_info.path)
        # now try to find and initialise the first subclass of the correct plugin interface


        for element in [getattr(candidate_module,name) for name in dir(candidate_module)]:
            plugin_info_reference = None
            for category_name in self.categories_interfaces:
                try:
                    is_correct_subclass = issubclass(element, self.categories_interfaces[category_name])
                except TypeError:
                    continue
                if is_correct_subclass and element is not self.categories_interfaces[category_name]:
                        current_category = category_name
                        if candidate_infofile not in self._category_file_mapping[current_category]:
                            # we found a new plugin: initialise it and search for the next one
                            if not plugin_info_reference:
                                plugin_info.plugin_object = element(parent=self.parent,settings=self.settings)
                                plugin_info_reference = plugin_info
                            plugin_info.categories.append(current_category)
                            self.category_mapping[current_category].append(plugin_info_reference)
                            self._category_file_mapping[current_category].append(candidate_infofile)
    # Remove candidates list since we don't need them any more and
    # don't need to take up the space
    #delattr(self, '_candidates')
    return processed_plugins
