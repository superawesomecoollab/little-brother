from yapsy.IPlugin import IPlugin

"""Simple plugin architecture for FlyBlobTrack based on Yapsy.

    Classes that inherit from Pre- and Post-processing base classes must implement
    one method and should implement three more.  The required method is run(image), which accepts a BGR OpenCV
    image and returns a BGR OpenCV image.  Optional methods are:

        options(), which has no arguments but returns a QDialog object that can be
        shown by the GUI thread calling the plugin.  This dialog object will allow
        each plugin to have its own options interface that the GUI can display
        when the user chooses to set options.

        activate() / decativate(), which are called when the plugin is activated
        and decatived.  Most set up and tear down should be implemented in these
        methods, as well as error handling. Note that the super class toggles
        an instance variable, is_activated, in these methods.  Derived classes
        should either provide this variable or call the super methods (see template
        plugin for an example).

        startVideo(video), which will be passed a pointer to the entire loaded
        video and which must return a QThread object.  This QThread must implement a
        single method that takes no arguments, doWork(), that the GUI will call
        to allow background processing tasks from filters while the video is loading.
        Note that the GUI will throttle these threads and execute them sequentially
        to avoid blocking the GUI main thread as much as possible. See averageBackground.py
        for an example of this in action.

    Detection, assignment, and filtering classes must implement the same method as the Pre- and Post-processing
    classes. However, they must also implement a stats method specific to each type of plugin.

        detections(): this method will be called on all detection plugins, so that the data
        model can record detected blobs and pass them to assignment and filtering methods.
        The method must return a Pandas dataframe with the following columns: BlobNumber,
        CenterX, CenterY, Length, Width, Area, and Orientation.  The centroid, length / width and
        area measurements can be from any container (raw contours, bounding boxes, ellipses, etc.).
        Orientation measurements are radians from the horizontal.

        detectionPixels(): this method will be called on detection plugins, so that
        assignment methods that require contour data will not have to do a connected
        component search themselves.  The method must reuturn an array of numpy arrays,
        one array per contour holding that contour's pixels.

        assignments(): this method will be called on all assignment plugins.

        filtered(): this method will be called on all filtering plugins.


    Additional notes:
      Any plugin class that defines an attribute called 'rawFrame' will have
      a pointer to the current raw frame on every invocation.

      Any plugin class that defines an attribute called 'frameCount' will have
      the number of frames stored on every invocation.

      Any assignment plugin class that defines an attribute called 'contourPixels' will
      have a pointer to the pixels of all detected blobs on every invocation.

      Any plugin class that defines an attribute called 'labelstackRecord' will receive the
      current labelstack on every invocation.

      Any plugin class that defines an attribute called 'tracksRecord' will receive the
      entire blobtracking record on every invocation.

      Any plugin class that defines an attribute called 'detections' will receive the
      current frame's detections on every invocation.
    """

class IPreprocessingPlugin(IPlugin):
  def startVideo(self,video):
    raise NotImplementedError

class IPostprocessingPlugin(IPlugin):
  def startVideo(self,video):
    raise NotImplementedError

class IDetectionPlugin(IPlugin):
  def startVideo(self,video):
    raise NotImplementedError

class IAssignmentPlugin(IPlugin):
  def startVideo(self,video):
    raise NotImplementedError

class IFilteringPlugin(IPlugin):
  def startVideo(self,video):
    raise NotImplementedError

class IOutputPlugin(IPlugin):
  def startVideo(self,video):
    raise NotImplementedError
