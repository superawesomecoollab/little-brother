from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class BlobTrackerWidget(QWidget):
	def __init__(self):
		QWidget.__init__(self)
		self.model = QStandardItemModel(self)
		self.tableView = QTableView(self)
		self.tableView.setModel(self.model)
		self.tableView.horizontalHeader().setStretchLastSection(True)

		self.layoutVertical = QVBoxLayout(self)
		self.layoutVertical.addWidget(self.tableView)


	def update(self,tracksDF):
		try:
			for count,row in tracksDF.iterrows():
				tablerow = [ QStandardItem(str(x[1])) for x in list(row.iteritems()) ]
				self.model.appendRow(tablerow)
			self.tableView.scrollToBottom()
		except (ValueError,AttributeError) as e:
			pass

	def reset(self,labels):
		self.model.setHorizontalHeaderLabels(labels)
