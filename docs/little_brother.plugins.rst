little_brother.plugins package
==============================

Submodules
----------

little_brother.plugins.adaptiveMedianBGS module
-----------------------------------------------

.. automodule:: plugins.adaptiveMedianBGS
    :members:
    :undoc-members:
    :show-inheritance:


little_brother.plugins.averageBackground module
-----------------------------------------------

.. automodule:: plugins.averageBackground
    :members:
    :undoc-members:
    :show-inheritance:


little_brother.plugins.averageWorkerThread module
-------------------------------------------------

.. automodule:: plugins.averageWorkerThread
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.backgroundSubtraction module
---------------------------------------------------

.. automodule:: plugins.backgroundSubtraction
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.backgroundSubtraction2 module
----------------------------------------------------

.. automodule:: plugins.backgroundSubtraction2
    :members:
    :undoc-members:
    :show-inheritance:


little_brother.plugins.backgroundSubtractionGMG module
------------------------------------------------------

.. automodule:: plugins.backgroundSubtractionGMG
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.closing module
-------------------------------------

.. automodule:: plugins.closing
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.colorDetection module
--------------------------------------------

.. automodule:: plugins.colorDetection
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.connectedComponentLabel module
-----------------------------------------------------

.. automodule:: plugins.connectedComponentLabel
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.csvOutput module
---------------------------------------

.. automodule:: plugins.csvOutput
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.dbops module
-----------------------------------

.. automodule:: plugins.dbops
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.derivatives module
-----------------------------------------

.. automodule:: plugins.derivatives
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.dilation module
--------------------------------------

.. automodule:: plugins.dilation
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.ellipseFitting module
--------------------------------------------

.. automodule:: plugins.ellipseFitting
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.erosion module
-------------------------------------

.. automodule:: plugins.erosion
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.foregroundExtraction module
--------------------------------------------------

.. automodule:: plugins.foregroundExtraction
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.graph module
-----------------------------------

.. automodule:: plugins.graph
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.graphTrack module
----------------------------------------

.. automodule:: plugins.graphTrack
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.greyscale module
---------------------------------------

.. automodule:: plugins.greyscale
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.harris module
------------------------------------

.. automodule:: plugins.harris
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.hungarian module
---------------------------------------

.. automodule:: plugins.hungarian
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.hungarianAlgorithm module
------------------------------------------------

.. automodule:: plugins.hungarianAlgorithm
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.labelstackAssignment module
--------------------------------------------------

.. automodule:: plugins.labelstackAssignment
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.laplacian module
---------------------------------------

.. automodule:: plugins.laplacian
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.mergeDetection module
--------------------------------------------

.. automodule:: plugins.mergeDetection
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.opening module
-------------------------------------

.. automodule:: plugins.opening
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.plugins.sexDetection module
------------------------------------------

.. automodule:: plugins.sexDetection
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: plugins
    :members:
    :undoc-members:
    :show-inheritance:
