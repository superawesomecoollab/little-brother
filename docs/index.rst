.. little_brother documentation master file, created by
   sphinx-quickstart on Tue Apr 28 15:30:36 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to little_brother's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 4

   little_brother


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

