little_brother package
======================

Subpackages
-----------

.. toctree::

    little_brother.plugins

Submodules
----------

little_brother.BlobTracker module
---------------------------------

.. automodule:: BlobTracker
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.BlobTrackerWidget module
---------------------------------------

.. automodule:: BlobTrackerWidget
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.DelegatePluginManager module
-------------------------------------------

.. automodule:: DelegatePluginManager
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.PluginBaseClasses module
---------------------------------------

.. automodule:: PluginBaseClasses
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.QtHandler module
-------------------------------

.. automodule:: QtHandler
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.Video module
---------------------------

.. automodule:: Video
    :members:
    :undoc-members:
    :show-inheritance:

little_brother.little_brother module
------------------------------------

.. automodule:: little_brother
    :members:
    :undoc-members:
    :show-inheritance:

