## Little Brother

Little Brother is an extensible visualization platform for computer vision tracking of small organisms. It was developed by the [Nuzhdin](http://nlab.usc.edu/Home.html) / [Marjoram](http://pibbs.usc.edu/faculty/profile/?fid=254) lab at the University of Southern California to aid in automated tracking of Drosophila.  

Little Brother uses a plugin-based architecture to allow for rapid prototyping of new approaches to tracking.  Plugins - along with Little Brother itself - are written in [Python](https://www.python.org/).

[![Build Status](http://ec2-52-25-16-247.us-west-2.compute.amazonaws.com:8080/buildStatus/icon?job=little-brother)](http://ec2-52-25-16-247.us-west-2.compute.amazonaws.com:8080/job/little-brother/)

### Why "Little Brother"?

It's like Big Brother for little organisms.

### Dependencies:

Little Brother relies on several large libraries to operate.  These libraries must currently be installed by the user for use with *Python 2.7* (not 3.x):

* [Qt5](https://www.qt.io/download/)
* [PyQt5](http://www.riverbankcomputing.com/software/pyqt/download5).  Note that on many platforms, this must be installed from source as many distributions will link against Python 3.
* [OpenCV](http://opencv.org/).  As of this writing, this must be installed from source because it requires features only found in the 3.0.x branch of OpenCV.  With the recent release of a 3.0 release candidate, however, this might soon be installable by your system's package manager.  For instance, on OS X using the [Homebrew](http://brew.sh/) package manager, you can install the development version of opencv with ```brew install opencv --HEAD```.

Little Brother uses a plugin system to allow users to write their own code for processing videos.  The current set of published plugins is maintained as a [separate repository](https://bitbucket.org/superawesomecoollab/little-brother-plugins) and exists within this repository as a subtree.

### Installation:

With Qt5, PyQt5 and OpenCV successfully installed, other libraries required by the software may be installed by running 


```
#!python

python setup.py install
```

from the command line in the directory that you downloaded or cloned Little Brother.  However, if automated setup fails, you can also install the required libraries manually using *pip*:

* numpy, scipy, matplotlib, networkx, cython, python-igraph, ipython, pandas, yapsy, scikit-learn and scikit-image.

### Documentation & Development

API documentation is hosted on [ReadTheDocs](http://little-brother.readthedocs.org).  

Pull requests for improvements to the code or documentation are very welcome.

The plugins directory is now maintained as a git subtree.  To push or pull to subtree, you can add it as a remote:

    git remote add plugins git@bitbucket.org:superawesomecoollab/little-brother-plugins.git

If you want to get the latest plugin changes, please use (note that this must be run from the top-level directory in the repo):

    git subtree pull --prefix=little_brother/plugins --squash plugins master

If you make changes to the plugin code, please commit to the upstream this way:

    git subtree push --prefix=little_brother/plugins --squash plugins master

The core little-brother code is now maintained as a git subtree as well.  To pull the latest core changes:

    git subtree pull --prefix=little_brother/core/ --squash core master

And to commit upstream:

    git subtree push --prefix=little_brother/core/ --squash core master



### Running:

```
#!bash

cd little_brother
python little_brother.py
```

### Credits and contact:

Development of this software was led by [Steven Hamblin](http://winawer.org) ([github](http://github.com/Winawer)) and Brad Foley of the Nuzhdin lab.

### License

This software is licensed under the MIT license.  Please see the LICENSE file for more.