import pytest
import cv2
import numpy as np
import ConfigParser

@pytest.fixture(scope="module")
def gs_image():
	return cv2.imread("test/data/test_image_1.png")

@pytest.fixture(scope="module")
def color_image():
	return cv2.imread("test/data/test_image_2.png")
	
@pytest.fixture(scope="module")
def dummy_bgr_image():
    b=np.array([[100,0,0],[100,0,0],[0,0,0]], dtype=np.uint8)
    g=np.array([[100,0,0],[0,0,0],[0,0,0]], dtype=np.uint8)
    r=np.array([[0,0,0],[0,0,100],[0,0,100]], dtype=np.uint8)
    dummy_bgr_image=cv2.merge([b,g,r])    
    return dummy_bgr_image	

@pytest.fixture(scope="module")
def dummy_bgr_image1():
    b=np.array([[0,100,0],[100,0,100],[0,100,0]], dtype=np.uint8)
    g=np.array([[0,100,0],[100,0,100],[0,100,0]], dtype=np.uint8)
    r=np.array([[0,100,0],[100,0,100],[0,100,0]], dtype=np.uint8)
    dummy_bgr_image1=cv2.merge([b,g,r])    
    return dummy_bgr_image1  

@pytest.fixture(scope="module")
def settings():
    settings = ConfigParser.ConfigParser()
    settings.read("./test/data/test_settings.cfg")
    settings.file_path = "./test/data/test_settings.cfg"
    return settings


