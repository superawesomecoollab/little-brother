import pytest
from core.blob_tracker import BlobTracker
import pandas as pd

@pytest.fixture
def blobtracker_instance():
	blobtracker_instance = BlobTracker(cli=False,widget=None)
	blobtracker_instance.loadTracks("test/data/tracks.csv")
	return blobtracker_instance

@pytest.fixture
def fake_detections():
	df = pd.DataFrame(columns = ["Frame","BlobNumber","CenterX","CenterY","Height","Width","Area","Angle"])
	df.loc[0] = [1000,100,200,200,30,50,4712.39,38]
	return df

def test_reset(blobtracker_instance):
	"""Resetting should make the blob tracker have a blank DataFrame."""
	blobtracker_instance.reset()
	assert len(blobtracker_instance.detections.index) == 0		
	assert len(blobtracker_instance.blobTracks.index) == 0
	assert len(blobtracker_instance.labelstack.index) == 0

def test_get_tracks(blobtracker_instance):
	"""Should return a pandas data frame."""
	assert isinstance(blobtracker_instance.getTracks(),pd.DataFrame)
	assert list(blobtracker_instance.getTracks().columns.values) == blobtracker_instance.getColumns(dfType="tracks")

def test_next_id(blobtracker_instance):
	"""Test data has a max id of 7, so nextID should return 8.  

	Should return 1 after reset."""
	assert blobtracker_instance.nextID() == 8
	blobtracker_instance.reset()
	assert blobtracker_instance.nextID() == 1

def test_record_detections(blobtracker_instance,fake_detections):
	blobtracker_instance.recordDetections(fake_detections)
	detections = blobtracker_instance.getDetections()
	assert detections.tail(1).loc[0].equals(fake_detections.loc[0])
