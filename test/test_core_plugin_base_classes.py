import pytest
from core.plugin_base_classes import *

def test_all_startvideo():
	classes = [IPreprocessingPlugin, IPostprocessingPlugin,
			   IDetectionPlugin, IAssignmentPlugin,
			   IFilteringPlugin, IOutputPlugin]

	for class_ in classes:
		plugin = class_()
		with pytest.raises(NotImplementedError):
			plugin.startVideo(None)

