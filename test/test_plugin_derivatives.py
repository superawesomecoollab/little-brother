import pytest
import cv2
from derivatives import Derivatives
import numpy as np

"""
The test of the Sobel derivative output is a bit circular
The solution is taken by implementing the cv2.Sobel 
Mainly because we can't figure out exactly how the function is calculating the derivatives.
"""
@pytest.fixture(scope="module")
def sobel_out():
    sobel=np.array([[0,128,0],[128,192,128],[0,128,0]], dtype=np.uint8)
    sobel_out=cv2.cvtColor(sobel,cv2.COLOR_GRAY2BGR)
    return sobel_out	

@pytest.fixture
def derivatives_instance(settings):
	return Derivatives(None,settings)

def test_activation(derivatives_instance):
	derivatives_instance.activate()
	assert derivatives_instance.is_activated

def test_deactivate(derivatives_instance):
	derivatives_instance.deactivate()
	assert not derivatives_instance.is_activated
	
def test_loadSettings(derivatives_instance, settings):
    assert derivatives_instance.kernelSize == 3
    assert derivatives_instance.xorder == 1
    assert derivatives_instance.yorder == 1
    assert derivatives_instance.xdirection == True    
    assert derivatives_instance.ydirection == True    

def test_returns_image(derivatives_instance,color_image):
	assert derivatives_instance.run(color_image).shape == color_image.shape
	

def test_returns_image_sobel(derivatives_instance,dummy_bgr_image,sobel_out):
    deriv_out=derivatives_instance.run(dummy_bgr_image)
    assert np.array_equal(deriv_out, sobel_out)

	
