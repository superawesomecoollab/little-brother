import pytest
from closing import closing


@pytest.fixture
def closing_instance(settings):    
	return closing(None,settings)

def test_activation(closing_instance):
	closing_instance.activate()
	assert closing_instance.is_activated

def test_deactivate(closing_instance):
	closing_instance.activate()
	closing_instance.deactivate()
	assert not closing_instance.is_activated

def loadSettings(closing_instance,settings):
	assert closing_instance.kernelSize == 3
	assert closing_instance.elementValue == 0
	assert closing_instance.kernel == cv2.getStructuringElement(closing.elementValue,(7,7))

def run(closing_instance):
	assert closing_instance.run(color_image).shape == color_image.shape
	assert closing._instance.run(dummy_bgr_image1)==cv2.morphologyEx(image, cv2.MORPH_CLOSE, closing.kernel)

