import pytest
import cv2
from greyscale import Greyscale


@pytest.fixture
def greyscale_instance():
	return Greyscale(None,None)

def test_activation(greyscale_instance):
	greyscale_instance.activate()
	assert greyscale_instance.is_activated

def test_deactivate(greyscale_instance):
	greyscale_instance.activate()
	greyscale_instance.deactivate()
	assert not greyscale_instance.is_activated

def test_returns_image(greyscale_instance,color_image):
	assert greyscale_instance.run(color_image).shape == color_image.shape

def test_returned_image_is_greyscale(greyscale_instance,color_image):
	# If the image is grey scale, all three channels will be set to the same value.
	# Thus, summing across all channels and dividing by 3 should be the same as summing one channel.
	gs = greyscale_instance.run(color_image)
	assert int(gs.sum()/3) == gs[:,:,0].sum()

