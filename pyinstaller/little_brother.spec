# -*- mode: python -*-
hiddenimports = [ 'skimage.color',
                  'skimage.color.colorconv',
                  'skimage.color.colorlabel',
                  'skimage.color.rgb_colors',
                  'skimage.color.delta_e',
                  'skimage.color.adapt_rgb',                  
                  'skimage.draw',
                  'skimage.draw.draw',
                  'skimage.draw._draw',
                  'skimage.draw.draw3d',
                  'skimage.exposure',
                  'skimage.exposure.exposure',
                  'skimage.exposure._adapthist',
                  'skimage.filters',
                  'skimage.filters.lpi_filter',
                  'skimage.filters._gaussian',                  
                  'skimage.filters.edges',
                  'skimage.filters._rank_order',
                  'skimage.filters._gabor',
                  'skimage.filters.thresholding',
                  'skimage.filters.rank',
                  'skimage.filters.rank.generic',
                  'skimage.filters.rank.core_cy',
                  'skimage.filters.rank.generic_cy',
                  'skimage.filters.rank._percentile',
                  'skimage.filters.rank.percentile_cy',
                  'skimage.filters.rank.bilateral',
                  'skimage.filters.rank.bilateral_cy',                  
                  'skimage.measure',
                  'skimage.measure._find_contours',
                  'skimage.measure._find_contours_cy',
                  'skimage.measure._marching_cubes',
                  'skimage.measure._marching_cubes_cy',
                  'skimage.measure._regionprops',
                  'skimage.measure._label',
                  'skimage.measure._ccomp',
                  'skimage.measure._moments',
                  'skimage.measure._moments_cy',
                  'skimage.measure._structural_similarity',
                  'skimage.measure._polygon',
                  'skimage.measure._pnpoly',
                  'skimage.measure.profile',
                  'skimage.measure.fit',
                  'skimage.measure.block',
                  'skimage.morphology',                  
                  'skimage.morphology._watershed',
                  'skimage.morphology._skeletonize',
                  'skimage.morphology._skeletonize_cy',                  
                  'skimage.morphology._convex_hull',
                  'skimage.morphology.convex_hull',
                  'skimage.morphology.greyreconstruct',
                  'skimage.morphology.binary',
                  'skimage.morphology.misc',
                  'skimage.morphology.selem',
                  'skimage.morphology.grey',
                  'skimage.morphology.watershed',
                  'skimage.restoration',
                  'skimage.restoration.deconvolution',
                  'skimage.restoration.uft',
                  'skimage.restoration.unwrap',
                  'skimage.restoration._unwrap_1d',
                  'skimage.restoration._unwrap_2d',
                  'skimage.restoration._unwrap_3d',
                  'skimage.restoration._denoise',
                  'skimage.restoration._denoise_cy',
                  'skimage.restoration.non_local_means',
                  'skimage.restoration._nl_means_denoising',
                  'skimage.segmentation',                  
                  'skimage.segmentation.random_walker_segmentation',
                  'skimage.segmentation._felzenszwalb',
                  'skimage.segmentation._felzenszwalb_cy',
                  'skimage.segmentation.slic_superpixels',
                  'skimage.segmentation._slic',
                  'skimage.segmentation._quickshift',
                  'skimage.segmentation.boundaries',
                  'skimage.segmentation._clear_border',
                  'skimage.segmentation._join',                  
                  'skimage.util.dtype',                  
                  'skimage._shared',
                  'skimage._shared.utils',
                  'skimage._shared._warnings',
                  'skimage._shared.geometry',
                  'sklearn',
                  'sklearn.__check_build',
                  'sklearn.__check_build._check_build',
                  'sklearn.base',
                  'sklearn.externals',
                  'sklearn.externals.six',
                  'sklearn.ensemble',
                  'sklearn.ensemble.base',
                  'sklearn.utils',
                  'sklearn.utils.murmurhash',
                  'sklearn.utils.validation',
                  'sklearn.utils.class_weight',
                  'sklearn.utils.fixes',
                  'sklearn.externals.joblib',
                  'sklearn.externals.joblib.memory',
                  'sklearn.externals.joblib.hashing',
                  'sklearn.externals.joblib.func_inspect',
                  'sklearn.externals.joblib._compat',
                  'sklearn.externals.joblib.logger',
                  'sklearn.externals.joblib.disk',
                  'sklearn.externals.joblib.numpy_pickle',
                  'sklearn.externals.joblib.parallel',
                  'sklearn.externals.joblib._multiprocessing_helpers',
                  'sklearn.externals.joblib.pool',
                  'sklearn.externals.joblib.format_stack',
                  'sklearn.externals.joblib.my_exceptions',
                  'sklearn.ensemble.forest',
                  'sklearn.feature_selection',
                  'sklearn.feature_selection.univariate_selection',
                  'sklearn.preprocessing',
                  'sklearn.preprocessing.data',
                  'sklearn.utils.extmath',
                  'sklearn.utils._logistic_sigmoid',
                  'sklearn.utils.sparsefuncs_fast',
                  'sklearn.utils.sparsefuncs',
                  'sklearn.preprocessing.label',
                  'sklearn.utils.multiclass',
                  'sklearn.preprocessing.imputation',
                  'sklearn.feature_selection.base',
                  'sklearn.feature_selection.variance_threshold',
                  'sklearn.feature_selection.rfe',
                  'sklearn.utils.metaestimators',
                  'sklearn.cross_validation',
                  'sklearn.metrics',
                  'sklearn.metrics.ranking',
                  'sklearn.utils.stats',
                  'sklearn.metrics.base',
                  'sklearn.metrics.classification',
                  'sklearn.metrics.cluster',
                  'sklearn.metrics.cluster.supervised',
                  'sklearn.utils.lgamma',
                  'sklearn.metrics.cluster.expected_mutual_info_fast',
                  'sklearn.metrics.cluster.unsupervised',
                  'sklearn.metrics.pairwise',
                  'sklearn.metrics.pairwise_fast',
                  'sklearn.metrics.cluster.bicluster',
                  'sklearn.utils.linear_assignment_',
                  'sklearn.metrics.regression',
                  'sklearn.metrics.scorer',
                  'sklearn.feature_selection.from_model',
                  'sklearn.tree',
                  'sklearn.tree.tree',
                  'sklearn.tree._utils',
                  'sklearn.tree._tree',
                  'sklearn.tree.export',
                  'sklearn.ensemble.bagging',
                  'sklearn.utils.random',
                  'sklearn.utils._random',
                  'sklearn.ensemble.weight_boosting',
                  'sklearn.ensemble.gradient_boosting',
                  'sklearn.ensemble._gradient_boosting',
                  'sklearn.ensemble.partial_dependence',                  
                  'scipy.special._ufuncs_cxx',  
                  'igraph',
                  'igraph._igraph',
                  'igraph.clustering',
                  'igraph.compat',
                  'igraph.configuration',
                  'igraph.datatypes',
                  'igraph.drawing',
                  'igraph.drawing.colors',
                  'igraph.utils',
                  'igraph.drawing.graph',
                  'igraph.drawing.baseclasses',
                  'igraph.drawing.utils',
                  'igraph.drawing.edge',
                  'igraph.drawing.metamagic',
                  'igraph.drawing.text',
                  'igraph.drawing.shapes',
                  'igraph.drawing.vertex',
                  'igraph.layout',
                  'igraph.statistics',
                  'igraph.summary',
                  'igraph.vendor',
                  'igraph.vendor.texttable',
                  'igraph.cut',
                  'igraph.formula',
                  'igraph.matching',
                  'igraph.remote',
                  'igraph.remote.nexus',
                  'numpy.core.umath',
                  'numpy.core.umath_tests',
                  ]

a = Analysis(['../little_brother/little_brother.py'],
             pathex=['../little_brother', '../little_brother/plugins', '/Users/winawer/Dropbox/Lab/Projects/little-brother/pyinstaller'],
             hiddenimports=hiddenimports,
             hookspath=None,
             runtime_hooks=None)

a.datas += [('settings.cfg','../little_brother/settings.cfg','DATA')]
plugins_tree = Tree('../little_brother/plugins',prefix='plugins')
a.datas += plugins_tree

pyz = PYZ(a.pure)

exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='little_brother',
          debug=False,
          strip=None,
          upx=True,
          console=True )

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='little_brother')
app = BUNDLE(coll,
             name='little_brother.app',
             icon=None)
